#include "SmogHisto.h"


using namespace std;

BaseHisto::BaseHisto(string sname)  {
  thename=sname; //prefix 
  Debug=0;
}

BaseHisto::~BaseHisto() {}

int BaseHisto::fillh1(string sname, double x, double weight=1.) {
  if(Debug) cout <<thename<<">> myHisto::fillh1() "<<sname<< endl;
string name="h1"+thename+"_"+sname;
 for (unsigned int i=0; i <thevh.size(); i++)
 {
    if (thevh.at(i)->GetName()==(name))
    {
			thevh.at(i)->Fill(x, weight);
			return 1;
    }   
 }
 if(Debug)  cout<<" ** not found h1**  "<<name<<" "<<sname<<endl;
  return 0;  
}
//
int BaseHisto::fillh3(string sname, double x, double y, double z, double weight=1.) {
string name="h3"+thename+"_"+sname;
 for (unsigned int i=0; i <thevh3.size(); i++)
 {
    if (thevh3.at(i)->GetName()==(name))
    {
      thevh3.at(i)->Fill(x, y, z, weight);
			return 1;
    }
 }
  return 0;  
}
//
int BaseHisto::fillh2(string sname, double x, double y, double weight=1.) {
string name="h2"+thename+"_"+sname;
 for (unsigned int i=0; i <thevh2.size(); i++)
 {
      if (thevh2.at(i)->GetName()==(name))
    {
			thevh2.at(i)->Fill(x, y, weight);
			return 1;
    }
 }
  return 0;  
}
// write
int BaseHisto::write(string fname) {
  if(Debug) cout <<">> myHisto::write() "<<thename<< endl;
  TFile*  af   = new TFile(fname.c_str(), "UPDATE" );
  if(!af->IsOpen()) 
    {if(Debug) cout <<"***cant write to root file "<<fname<< endl;return 0; } 
  return write(af);
 }

int BaseHisto::write(TFile* afile) {
 if(Debug) cout  <<">> myHisto::write() "<<thename<< endl;

 TDirectory* df=afile->mkdir(thename.c_str());
 df->cd(); 
 for (unsigned int i=0; i <thevh.size(); i++)
 {
   if(Debug)  cout <<thename<<" write h1 "<< thevh.at(i)->GetName()<< endl;
 thevh.at(i)->Write();
 }
 for (unsigned int i=0; i <thevh2.size(); i++)
 {
   if(Debug)  cout <<thename<<" write h2 "<< thevh2.at(i)->GetName()<< endl;   
   thevh2.at(i)->Write();
 }
for (unsigned int i=0; i <thevh3.size(); i++)
 {
    if(Debug)  cout <<thename<<" write h3 "<< thevh3.at(i)->GetName()<< endl;   
   thevh3.at(i)->Write();
 }
   if(Debug)  cout<<thename<<">> myHisto::write done "<<" h1="<<thevh.size()<<" h2="<<thevh2.size()<< endl;
 return 1;  
} 
//
void BaseHisto::clean()  {
  // clean up exisiting
  for (unsigned int i=0; i <thevh.size(); i++) delete thevh.at(i);
 for (unsigned int i=0; i <thevh2.size(); i++)  delete thevh2.at(i);
 for (unsigned int i=0; i <thevh3.size(); i++) delete thevh3.at(i);
thevh.clear();
thevh2.clear();
thevh3.clear();
 return;
}
/////////////////
MyHisto::MyHisto(string sname) :
  BaseHisto(sname)  {
 Debug=0;
  book();
}
MyHisto::MyHisto(string sname, int amc) :
  BaseHisto(sname)  {
 Debug=0;
  bmc=amc;
  book();
}
MyHisto::~MyHisto() {}
//
int MyHisto::book() {
  clean();
 /////fill new
int nb=50;double dn=0.; double un=100.;
int npvz=100;double dpvz=-1000.; double upvz=600.;
int np=100;double dp=0.; double up=200000.; //in MeV
int npt=100;double dpt=0.; double upt=20000.; //
int neta=30;double deta=1.; double ueta=6.; //
int ndll=100; double ddll=-150; double udll=150.; //
int npvz3=50;double dpvz3=-350.; double upvz3=150.;
int ntrk3=10;double dntrk3=0.0; double untrk3=50.0;
int nmm=100; double dmm=1060.; double umm=1200.;
// note binning
int nngp=19;
double rbgp[19]={12000.,14000.,16200.,18700.,21400.,24400.,27700.,31400.,
                 35500.,40000.,45000.,50500.,56700.,63500.,71000.,79300.,88500.,98700.,110000.};
int nngpt=13;
double rbgpt[13]={0.,200.,400.,550.,700.,800.,900.,1050.,1200.,1500.,
                 1980.,2760.,4000.};

int nngntrk=6;
double rbgntrk[6]={0.,10.,20.,40.,60.,80.};  
int nnngvz=6;
double rbgvz[6]={-700,-400.,-200.,-100.,0.,100.};  

string spref1="h1"+thename+"_";
string spref2="h2"+thename+"_";
string spref3="h3"+thename+"_";
thevh.reserve(5);
thevh2.reserve(20);
{const char* name=(spref1+"nltrk").c_str();TH1D* h1 = new TH1D(name,name,nb,dn,un);thevh.push_back(h1); }
{const char* name=(spref1+"vz").c_str();TH1D* h1 = new TH1D(name,name,npvz,dpvz,upvz);thevh.push_back(h1); }
{const char* name=(spref1+"p").c_str();TH1D* h1 = new TH1D(name,name,np,dp,up);thevh.push_back(h1); }
{const char* name=(spref1+"pt").c_str();TH1D* h1 = new TH1D(name,name,npt,dpt,upt);thevh.push_back(h1); }
{const char* name=(spref1+"eta").c_str();TH1D* h1 = new TH1D(name,name,neta,deta,ueta);thevh.push_back(h1); }
 if(!bmc) {
TH2D* h2_1=new TH2D((spref2+"pidk2pidp").c_str(),(spref2+"pidk2pidp").c_str(),ndll,ddll,udll,ndll,ddll,udll);
thevh2.push_back(h2_1);
 }
TH2D* h2_2=new TH2D((spref2+"p2pt").c_str(),(spref2+"p2pt").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);
thevh2.push_back(h2_2);
TH2D* h2_3=new TH2D((spref2+"p2pt2z_m700").c_str(),(spref2+"p2pt2z_m700").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);
thevh2.push_back(h2_3);
TH2D* h2_4=new TH2D((spref2+"p2pt2z_m400").c_str(),(spref2+"p2pt2z_m400").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);
thevh2.push_back(h2_4);
TH2D* h2_5=new TH2D((spref2+"p2pt2z_m200").c_str(),(spref2+"p2pt2z_m200").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);
thevh2.push_back(h2_5);
TH2D* h2_6=new TH2D((spref2+"p2pt2z_m100").c_str(),(spref2+"p2pt2z_m100").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);
thevh2.push_back(h2_6);
TH2D* h2_7=new TH2D((spref2+"p2pt2z_mp0").c_str(),(spref2+"p2pt2z_p0").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);
thevh2.push_back(h2_7);
TH2D* h2_8=new TH2D((spref2+"p2pt2n_0").c_str(),(spref2+"p2pt2n_0").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);
thevh2.push_back(h2_8);
TH2D* h2_9=new TH2D((spref2+"p2pt2n_10").c_str(),(spref2+"p2pt2n_10").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);
thevh2.push_back(h2_9);
TH2D* h2_10=new TH2D((spref2+"p2pt2n_20").c_str(),(spref2+"p2pt2n_20").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);
thevh2.push_back(h2_10);
TH2D* h2_11=new TH2D((spref2+"p2pt2n_40").c_str(),(spref2+"p2pt2n_40").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);
thevh2.push_back(h2_11);

// list the booked histo

if(Debug) { cout <<" +++list histo name "<<thename<< endl;
for (unsigned int i=0; i <thevh.size(); i++) 
  cout <<thevh.at(i)->GetName()<<endl;
for (unsigned int i=0; i <thevh2.size(); i++) 
   cout<<thevh2.at(i)->GetName()<<endl;
for (unsigned int i=0; i <thevh3.size(); i++) 
   cout<<thevh3.at(i)->GetName()<<endl;
 }

for (unsigned int i=0; i <thevh.size(); i++) thevh.at(i)->Sumw2();
for (unsigned int i=0; i <thevh2.size(); i++) thevh2.at(i)->Sumw2();
for (unsigned int i=0; i <thevh3.size(); i++) thevh3.at(i)->Sumw2();
return 1;
}

////
int MyHisto::fill(double p, double pt, double eta, double pidk, double pidp, double vz, int ntrk, double ww) {
    if(Debug)  cout <<thename<<" myHisto fill "<<p<<" "<<pt<<" "<<eta<<" "<<pidk<<" "<<pidp<<" "<<vz<<" "<<ntrk<<" "<<ww<< endl;
  
double  w=ww;
fillh1("p",p,w);
fillh1("pt",pt,w);
fillh1("eta",eta,w);
fillh1("vz",vz,w);
fillh1("nltrk",ntrk,w);

fillh2("p2pt",p,pt,w);
 if(!bmc) fillh2("pidk2pidp",pidk,pidp,w);
 if(vz>=-700&&vz< -400) fillh2("p2pt2z_m700",p,pt,w);
 if(vz>=-400&&vz< -200) fillh2("p2pt2z_m400",p,pt,w);
 if(vz>=-200&&vz< -100) fillh2("p2pt2z_m200",p,pt,w);
 if(vz>=-100&&vz> 0) fillh2("p2pt2z_m100",p,pt,w);
 if(vz>=00&&vz< 100) fillh2("p2pt2z_p0",p,pt,w);

  if(ntrk>=0&&ntrk< 10) fillh2("p2pt2n_0",p,pt,w);
  if(ntrk>=10&&ntrk< 20) fillh2("p2pt2n_10",p,pt,w);
  if(ntrk>=20&&ntrk< 40) fillh2("p2pt2n_20",p,pt,w);
  if(ntrk>=40) fillh2("p2pt2n_40",p,pt,w);
 
 return 1;
  }


// EventHisto
/////////////
MyEvHisto::MyEvHisto(string sname) :
  BaseHisto(sname)  {
 Debug=0;
  book();
}
MyEvHisto::MyEvHisto(string sname, int amc) :
  BaseHisto(sname)  {
 Debug=0;
  bmc=amc;
  book();
}

MyEvHisto::~MyEvHisto() {}
//
int MyEvHisto::book() {
  clean();
 /////fill new
int nb=50;double dn=0.; double un=100.;
int npvz=100;double dpvz=-1000.; double upvz=600.;
int np=100;double dp=0.; double up=200000.; //in MeV
int npt=100;double dpt=0.; double upt=20000.; //
int neta=30;double deta=1.; double ueta=6.; //
int ndll=100; double ddll=-150; double udll=150.; //
int npvz3=50;double dpvz3=-350.; double upvz3=150.;
int ntrk3=10;double dntrk3=0.0; double untrk3=50.0;
int nmm=100; double dmm=1060.; double umm=1200.;
// note binning
int nngp=19;
double rbgp[19]={12000.,14000.,16200.,18700.,21400.,24400.,27700.,31400.,
                 35500.,40000.,45000.,50500.,56700.,63500.,71000.,79300.,88500.,98700.,110000.};
int nngpt=13;
double rbgpt[13]={0.,200.,400.,550.,700.,800.,900.,1050.,1200.,1500.,
                 1980.,2760.,4000.};

int nngntrk=6;
double rbgntrk[6]={0.,10.,20.,40.,60.,80.};  
int nnngvz=6;
double rbgvz[6]={-700,-400.,-200.,-100.,0.,100.};  

string spref1="h1"+thename+"_";
string spref2="h2"+thename+"_";
string spref3="h3"+thename+"_";
thevh.reserve(5);
thevh2.reserve(20);
{const char* name=(spref1+"nltrk").c_str();TH1D* h1 = new TH1D(name,name,nb,dn,un);thevh.push_back(h1); }
{const char* name=(spref1+"nbtrk").c_str();TH1D* h1 = new TH1D(name,name,nb,dn,un);thevh.push_back(h1); }
{const char* name=(spref1+"npv").c_str();TH1D* h1 = new TH1D(name,name,nb,dn,un);thevh.push_back(h1); }
{const char* name=(spref1+"nmu").c_str();TH1D* h1 = new TH1D(name,name,nb,dn,un);thevh.push_back(h1); }
// vrtx
{const char* name=(spref1+"pvz").c_str();TH1D* h1 = new TH1D(name,name,npvz,dpvz,upvz);thevh.push_back(h1); }
{const char* name=(spref1+"pvstopo").c_str();TH1D* h1 = new TH1D(name,name,nb,0,2.0);thevh.push_back(h1); }
{const char* name=(spref1+"pvntrk").c_str();TH1D* h1 = new TH1D(name,name,nb,dn,un);thevh.push_back(h1); }
// all trks
{const char* name=(spref1+"trkp").c_str();TH1D* h1 = new TH1D(name,name,np,dp,up);thevh.push_back(h1); }
{const char* name=(spref1+"trkpt").c_str();TH1D* h1 = new TH1D(name,name,npt,dpt,upt);thevh.push_back(h1); }
{const char* name=(spref1+"trketa").c_str();TH1D* h1 = new TH1D(name,name,neta,deta,ueta);thevh.push_back(h1); }
TH2D* h2_1=new TH2D((spref2+"p2pt").c_str(),(spref2+"p2pt").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);thevh2.push_back(h2_1);
 if(bmc) {
TH2D* h2_2=new TH2D((spref2+"mcpb_p2pt").c_str(),(spref2+"mcpb_p2pt").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);thevh2.push_back(h2_2);
TH2D* h2_20=new TH2D((spref2+"mcpbnp_p2pt").c_str(),(spref2+"mcpbnp_p2pt").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);thevh2.push_back(h2_20);
TH2D* h2_3=new TH2D((spref2+"mcp_p2pt").c_str(),(spref2+"mcp_p2pt").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);thevh2.push_back(h2_3);
TH2D* h2_4=new TH2D((spref2+"mck_p2pt").c_str(),(spref2+"mck_p2pt").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);thevh2.push_back(h2_4);
TH2D* h2_5=new TH2D((spref2+"mcpi_p2pt").c_str(),(spref2+"mcpi_p2pt").c_str(),nngp-1,rbgp,nngpt-1,rbgpt);thevh2.push_back(h2_5);
 }
// list the booked histo

if(Debug) { cout <<" +++list histo name "<<thename<< endl;
for (unsigned int i=0; i <thevh.size(); i++) 
  cout <<thevh.at(i)->GetName()<<endl;
for (unsigned int i=0; i <thevh2.size(); i++) 
   cout<<thevh2.at(i)->GetName()<<endl;
for (unsigned int i=0; i <thevh3.size(); i++) 
   cout<<thevh3.at(i)->GetName()<<endl;
 }

for (unsigned int i=0; i <thevh.size(); i++) thevh.at(i)->Sumw2();
for (unsigned int i=0; i <thevh2.size(); i++) thevh2.at(i)->Sumw2();
for (unsigned int i=0; i <thevh3.size(); i++) thevh3.at(i)->Sumw2();
return 1;
}

////
int MyEvHisto::fillev(int nlongtracks,int nbacktracks,int nupstreamtracks,int npv,int nmuons) {
double  w=1.0;
fillh1("nltrk",nlongtracks,w);
fillh1("nbtrk",nbacktracks,w);
fillh1("npv",npv,w);
fillh1("nmu",nmuons,w);
 return 1;
  }

int MyEvHisto::fillvtx(double vz, double smogtopo, int ntrk) {
double  w=1.0;
fillh1("pvz",vz,w);
fillh1("pvstopo",smogtopo,w);
fillh1("pvntrk",ntrk,w);
  return 1;
}

int MyEvHisto::filltrk(double  p,  double pt, double eta, double ghost) {
double  w=1.0;
fillh1("trkp",p,w);
fillh1("trkpt",pt,w);
fillh1("trketa",eta,w);
fillh2("p2pt",p,pt,w);
 return 1;
}

int MyEvHisto::fillmcpb(double  p,  double pt, double eta) {
double  w=1.0;
fillh2("mcpb_p2pt",p,pt,w);
 return 1;
}
int MyEvHisto::fillmcpbnp(double  p,  double pt, double eta) {
double  w=1.0;
fillh2("mcpbnp_p2pt",p,pt,w);
 return 1;
}
int MyEvHisto::fillmcp(double  p,  double pt, double eta) {
double  w=1.0;
fillh2("mcp_p2pt",p,pt,w);
 return 1;
}
int MyEvHisto::fillmcpi(double  p,  double pt, double eta) {
double  w=1.0;
fillh2("mcpi_p2pt",p,pt,w);
 return 1;
}
int MyEvHisto::fillmck(double  p,  double pt, double eta) {
double  w=1.0;
fillh2("mck_p2pt",p,pt,w);
 return 1;
}
