
#ifndef SMOGANALYSIS_H
#define SMOGANALYSIS_H

#include "string"
#include "stdexcept"
#include "sstream"
#include "map"
#include "vector"
#include "cassert"
#include "functional"
#include "algorithm"
#include "limits"

// Gaudi

#include "GaudiKernel/ParticleProperty.h"
#include "GaudiKernel/IParticlePropertySvc.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/GaudiHistos.h"

//  DaVinci

#include "Kernel/DaVinciTupleAlgorithm.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "Kernel/ParticleFilters.h"
#include "Kernel/ICheckOverlap.h"
#include <Kernel/IDistanceCalculator.h>
#include <Kernel/IVertexFit.h>
// MC
#include "MCInterfaces/IMCReconstructible.h"
#include "MCInterfaces/IMCReconstructed.h"
#include "Particle2MCLinker.h" // using bug-fixed local version
//#include  "Kernel/Particle2MCParticle.h"
#include "Kernel/IMC2Collision.h"
#include "Kernel/Particle2MCMethod.h"
#include "Kernel/MCAssociation.h"
#include "Kernel/IParticle2MCAssociator.h"
// event information

#include "Event/Track.h"
#include "Event/ODIN.h"
#include "Event/HltDecReports.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/GenCollision.h"
#include "Event/RecSummary.h"

#include "Event/RawEvent.h"
#include "Event/RawBank.h"
#include "Event/VeloCluster.h"
#include "Event/STCluster.h"
#include "Event/CaloDigit.h"
#include "Event/VeloCluster.h"
#include "Event/STCluster.h"
#include "Event/OTTime.h"
#include "Event/RecVertex.h"

#include "Event/MCParticle.h"
#include "Event/MCHeader.h"
#include "Event/MCVertex.h"

// Velo
#include "VeloDet/DeVelo.h"
#include <VeloDet/DeVeloSensor.h> 
#include <VeloDet/DeVeloRType.h>


// Root

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TObjString.h"
#include "TMatrixT.h"
#include "TMatrixFSym.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"

// tree branch types definition
namespace {
  template <typename T> const char* leaf_desc();
  template <> const char* leaf_desc<int8_t>() { return "/B"; }
  template <> const char* leaf_desc<int16_t>() { return "/S"; }
  template <> const char* leaf_desc<int32_t>() { return "/I"; }
  template <> const char* leaf_desc<int64_t>() { return "/L"; }
  template <> const char* leaf_desc<uint8_t>() { return "/b"; }
  template <> const char* leaf_desc<uint16_t>() { return "/s"; }
  template <> const char* leaf_desc<uint32_t>() { return "/i"; }
  template <> const char* leaf_desc<uint64_t>() { return "/l"; }
  template <> const char* leaf_desc<float>() { return "/F"; }
  template <> const char* leaf_desc<double>() { return "/D"; }
}

/** @class SmogTree 
 *
 *
 *  @author V.Z.  modified from DstConverter
 *  @date   2017-26-04 
 */
class MyHisto;
class MyEvHisto;

class SmogAnalysis : public DaVinciTupleAlgorithm {

  // tree bracn management
  template <typename S>
  class array_mgr {

    class array_resizer_base {
    public:
      virtual ~array_resizer_base() {}
      virtual void resize(unsigned, unsigned) = 0;
    };

    template <typename T>
    class array_resizer : public array_resizer_base {
      T** data_;
      TBranch* branch_;
    public:
      array_resizer(T** ptr, TBranch* b) :
        data_(ptr), branch_(b)
      {
        *data_ = nullptr;
      }

      virtual ~array_resizer() { delete [] *data_; }

      virtual void resize(unsigned osize, unsigned nsize) {
        T* t = new T[nsize];
        std::copy(*data_, *data_ + osize, t);
        delete [] *data_;
        *data_ = t;
        branch_->SetAddress(t);
      }
    };

    std::vector<array_resizer_base*> resizer_;
    std::vector<TClonesArray*> clones_arrays_;
    S size_;
    S capacity_;
    TTree* tree_;
    std::string blen_;

    void grow() { resize(1.6 * capacity_); }

  public:
    using size_type = S;

    array_mgr() : size_(0), capacity_(0), tree_(nullptr) {}

    void set(TTree* t) { tree_ = t; }

    void size_branch(const char* name) {
      blen_ = name;
      std::string desc(name);
      desc += leaf_desc<S>();
      tree_->Branch(name, &size_, desc.c_str());
    }

    template <typename T>
    void branch(const char* name, T** arr_ptr) {
      std::ostringstream desc;
      desc << name << "[" << blen_ << "]" << leaf_desc<T>();
      TBranch* b = tree_->Branch(name, *arr_ptr, desc.str().c_str());
      resizer_.push_back(new array_resizer<T>(arr_ptr, b));
    }

    void branch(const char* name, TClonesArray** arr_ptr, const char* class_name) {
      (*arr_ptr) = new TClonesArray(class_name);
      tree_->Branch(name, arr_ptr);
      // this produces a complaint:
      // Warning in <TTree::Bronch>: Using split mode on a class: TMatrixTSym<float> with a custom Streamer
      // The warning cannot be avoided but may be ignored, see https://root.cern.ch/phpBB3/viewtopic.php?t=4536
      (*arr_ptr)->BypassStreamer(); // use this after call to Branch according to R. Brun
      clones_arrays_.push_back(*arr_ptr);
    }

    const S& increase() {
      if (size_ == std::numeric_limits<S>::max()) {
        std::ostringstream msg;
        msg << "cannot increase size of " << blen_ << " further, use a larger integer type";
        throw std::overflow_error(msg.str());
      }
      ++size_;
      if (size_ == capacity_)
        grow();
      return size_;
    }

    S pos() const { return size_; }

    void reset() {
      size_ = 0;
      for (auto& p : clones_arrays_)
        p->Clear();
    }

    void resize(unsigned c) {
      // std::assert(c > size_);
      for (auto r : resizer_)
        r->resize(capacity_, c);
      capacity_ = c;
    }
  };// class arr_mgr

public:

  SmogAnalysis( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~SmogAnalysis();

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode finalize  ();    ///< Algorithm finalization

private:

  using vtx_array_mgr = array_mgr<int16_t>;
  using trk_array_mgr = array_mgr<int16_t>;
  using mc_origins = std::vector<std::pair<trk_array_mgr::size_type, const LHCb::MCParticle*>>;
  using reco_origins = std::vector<std::pair<trk_array_mgr::size_type, const LHCb::Particle*>>;
 using proto_origins = std::vector<std::pair<trk_array_mgr::size_type, const LHCb::ProtoParticle*>>;
  //// fill methods
  void makeTree();
  void copyEventData();
  void copyLumiData();
  void copyVertexData();
  void copyTrackData();
  void copyVeloTrackData();
  void copyMCData();
  void matchIndx();
  void fillEventHisto();
  //// selection methods 
 int  selectSmog();  // select smog event
 bool hasProtoDuplicate(const LHCb::Particle * mc); // check duplicates
 int  pidSelect(const LHCb::Particle* part); // reco  pid cut selection
 int trackSelect(const LHCb::Particle* part); // track select
 int selectPV(const LHCb::RecVertex* rv ); // select good PV in smog
 double smogTopoall();                      // calculate smogTopo for all vertex
 double smogTopo(const LHCb::RecVertex* rv ); // calculate smogTopo for pv
 const LHCb::RecVertex* getPVOfTrack(const LHCb::Track* track, double& IP, double& IPE); // get best PV of track in smog
 const LHCb::RecVertex*   getPVOfTrackColl(const LHCb::Track * track, double& IP, double& IPE);  // same for collisions
 bool isInFiducial(const LHCb::Particle * ap);
 // MC selections
 bool isStableCharged(const LHCb::MCParticle * mc);
 bool isInFiducial(const LHCb::MCParticle * mc);
 const LHCb::ProtoParticle* getBestProto(const LHCb::MCParticle* mcp);
 int getMCpid(const LHCb::Particle* p);
 const LHCb::MCParticle*  getMCpart(const LHCb::Particle* p);
 const LHCb::MCVertex*   getMCvertex(const LHCb::RecVertex * rv);
 //
 void selectdowntracks(); 
 //
 int fselectsmog;
 //// parameters
  bool m_first_event;
  bool m_MC;
  bool m_coll;
  bool m_lumi;
  bool m_probnnx;
  bool m_velotrk;
  bool m_write_cov;
  bool m_smog;
  bool m_print_particles;
  //
  double m_minEta;
  double m_maxEta;
  double m_minP;
  double m_minPt;
  double m_maxZ;
  double m_minZ;
  double m_maxR;
  double m_maxIP;
  double m_maxch2IP;
  int    m_maxNpv;
  int    m_minNpv;
  int    m_maxNbktrk;
  double m_maxsmogtopo;
  double m_maxTrkGhostP;
  double m_maxTrkCh2;

  double m_thetaBeamX;
  double m_thetaBeamY;
  double m_posBeamX;
  double m_posBeamY;
  double m_maxdbeam;
  
  
  Particle2MCLinker* m_pLinker;
  const IMC2Collision* m_coll_tool;
  IMCReconstructible* m_recible;
  IMCReconstructed* m_rected;
  ProtoParticle2MCLinker* m_pLinkerProto;
 // MC associators
  std::vector<std::string> m_p2mcAssocTypes;
  std::vector<IParticle2MCAssociator*> m_p2mcAssocs;
  //duplicates
  std::vector<const LHCb::Particle*> vpd;
  // velo
  DeVelo*  fvelo;
  const LHCb::IParticlePropertySvc *m_ppSvc;
  const ILHCbMagnetSvc* m_magnetSvc;
  // selected
  std::vector<const LHCb::MCParticle*> themcpart;
  std::vector<const LHCb::Particle*> therpart;
  std::vector<const LHCb::ProtoParticle*> therproto;
  mc_origins mcparts;
  mc_origins mcrselparts;
  reco_origins rparts;
  proto_origins pparts;
  proto_origins pmcselparts;

  // histo
  MyHisto* thehall;
  MyHisto* thehsel;
  MyHisto* thehselpb;
  MyHisto* thehselp;
  MyHisto* thehselpi;
  MyHisto* thehselk;

  MyHisto*  thehallmc;
  MyHisto*  thehallmcpb;
  MyHisto*  thehallmcp;
  MyHisto*  thehallmcpi;
  MyHisto*  thehallmck;
  
  MyHisto*  thehaccmcpb;
  MyHisto*  thehricmcpb;
  MyHisto*  thehrecmcpb;

  MyHisto*  thehaccmck;
  MyHisto*  thehricmck;
  MyHisto*  thehrecmck;


  MyEvHisto*  thehevall;
  MyEvHisto*  thehevsel;

  //// Tree
  std::string m_filename;
  TFile * m_file;
  TTree * m_tree;
  //// tree
  int32_t evt_run;
  int64_t evt_evnum;
  int8_t  evt_bxtype,evt_trgtype;
  int16_t evt_bunchid;
  int8_t  evt_mag_pol;

  int32_t trig_tck;
  bool trig_bits[192];

  int16_t tracks_long, tracks_down, tracks_up,
          tracks_velo, tracks_t, tracks_back, tracks_any,
          tracks_ghost, tracks_muon;

  int16_t lumi_lc3, lumi_lc3b, lumi_lc14;

  int32_t hits_velo, hits_tt, hits_it, hits_ot, hits_spd,
          hits_rich1, hits_rich2, hits_muon;

  vtx_array_mgr m_vtx_mgr;
 
  float * vtx_x;
  float * vtx_y;
  float * vtx_z;
  float * vtx_chi2;
  int16_t * vtx_ndf;
  int16_t * vtx_ntrk;
  float *  vtx_smogtopo;
  float * vtx_mcz;
  int16_t * vtx_sel;
  TClonesArray* vtx_cov;

  trk_array_mgr m_trk_mgr;
  float * trk_px, * trk_py, * trk_pz;
  int16_t * trk_q;
  float *  trk_chi2;
  int16_t * trk_ndf;
  float * trk_dpovp;
  float * trk_pghost;
  float * trk_dllk, * trk_dllp, * trk_dllmu;
  int32_t * trk_selpid;
  int32_t * trk_mcpid;
  float * trk_pnnk, * trk_pnnmu;
  float * trk_ip, * trk_ipch2;
  float * trk_pvz;
  int16_t * trk_mcindx1;
  int16_t * trk_mcindx2;

  TClonesArray* trk_cov;

  trk_array_mgr m_vtrk_mgr;
  float * vtrk_px, * vtrk_py, * vtrk_pz,
        * vtrk_x, * vtrk_y, * vtrk_z,
        * vtrk_chi2;
  int16_t * vtrk_ndf;
  int8_t * vtrk_type;

  TClonesArray* vtrk_cov;

  vtx_array_mgr m_mc_vtx_mgr;
  float * mc_vtx_x;
  float * mc_vtx_y;
  float * mc_vtx_z;
  int8_t * mc_vtx_type;
  
  trk_array_mgr m_mc_trk_mgr;
  int16_t * mc_trk_store;
  float * mc_trk_px;
  float * mc_trk_py;
  float * mc_trk_pz;
  float * mc_trk_x;
  float * mc_trk_y;
  float * mc_trk_z;
  float * mc_trk_pvz;
  int32_t * mc_trk_pid;
  int32_t * mc_trk_mpid;
  int32_t * mc_trk_mmpid;
  int16_t * mc_trk_msel;
  int16_t * mc_trk_rindx1;
  int16_t * mc_trk_rindx2;
};


#endif // SMOGANALYSIS
