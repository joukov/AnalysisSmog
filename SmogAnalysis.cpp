#include "SmogAnalysis.h"
#include "SmogHisto.h"

using namespace Gaudi::Units;
using namespace LHCb;
using namespace std;

namespace {
  template <typename K, typename V>
  class default_map {
    using map = std::map<K, V>;
    map m_;
    const V def_;
  public:
    default_map(const V& def) : def_(def) {}
    V& operator[](const K& key) {
      auto it = m_.find(key);
      if (it == m_.end())
        return m_.insert(std::make_pair(key, -1)).first->second;
      return it->second;
    }
    void clear() { m_.clear(); }
  };

  template <typename T>
  TBranch* branch(TTree* tree, const char* name, T* val) {
    std::string desc(name);
    desc += leaf_desc<T>();
    return tree->Branch(name, val, desc.c_str());
  }
}

DECLARE_ALGORITHM_FACTORY( SmogAnalysis )

SmogAnalysis::SmogAnalysis(const std::string& name,
                           ISvcLocator* pSvcLocator) :
  DaVinciTupleAlgorithm(name , pSvcLocator),
  m_first_event(true)
{
  declareProperty("Filename", m_filename = "sct.root");
  declareProperty("Simulation", m_MC = false);
  declareProperty("Collisions", m_coll = false);
  declareProperty("WriteLumi", m_lumi = false);
  declareProperty("WriteProbNNx", m_probnnx = false);
  declareProperty("WriteCovariance", m_write_cov = false);
  declareProperty("WriteVeloTrk", m_velotrk = false);
  declareProperty("PrintParticles", m_print_particles = false);
  declareProperty("SelectSmog", m_smog = true);
  //selections
  declareProperty("minEta", m_minEta=1.5);
  declareProperty("maxEta", m_maxEta=5.5);
  declareProperty("minP", m_minP=10.0*Gaudi::Units::GeV);
  declareProperty("minPt", m_minPt=0.0*Gaudi::Units::GeV);
  declareProperty("maxZ", m_maxZ= 100.*Gaudi::Units::mm);
  declareProperty("minZ", m_minZ=-700.*Gaudi::Units::mm);
  declareProperty("maxR", m_maxR=1.4*Gaudi::Units::mm);
  declareProperty("maxIP", m_maxIP=2.0*Gaudi::Units::mm);
  declareProperty("maxch2IP", m_maxch2IP=12.);
  declareProperty("maxNpv", m_maxNpv=5);
  declareProperty("minNpv", m_minNpv=1);
  declareProperty("maxNbktrk", m_maxNbktrk=5);
  declareProperty("maxTrkGhostP", m_maxTrkGhostP=0.4);
  declareProperty("maxTrkCh2", m_maxTrkCh2=4.0); 
  // beam
  declareProperty("BeamCrossingAngleXZ", m_thetaBeamX = -0.490*Gaudi::Units::mrad, "Beam Crossing angle in XZ plane");
  declareProperty("BeamCrossingAngleYZ", m_thetaBeamY = -0.022*Gaudi::Units::mrad, "Beam Crossing angle in YZ plane");
  declareProperty("BeamSpotPositionX", m_posBeamX =  0.81*Gaudi::Units::mm, "Beam Spot X position at Z=0");
  declareProperty("BeamSpotPositionY", m_posBeamY =  -0.22*Gaudi::Units::mm, "Beam Spot Y position at Z=0");
  declareProperty("BeamPosition",m_maxdbeam=0.3*Gaudi::Units::mm, "PV distance to the beam at actual Z");
  declareProperty("maxSmogTopo",m_maxsmogtopo=0.4, "smog topo");
  //
  fselectsmog=0;

 if(m_MC) 
  {    
m_p2mcAssocTypes.push_back( "DaVinciSmartAssociator" );
m_p2mcAssocTypes.push_back( "MCMatchObjP2MCRelator"  );
declareProperty( "IP2MCPAssociatorType", m_p2mcAssocTypes );
  }
 m_pLinkerProto=0;
 m_pLinker=0;
 m_recible=0;
 m_rected=0;
 m_coll_tool=0;
  return;
}

SmogAnalysis::~SmogAnalysis() {}

void SmogAnalysis::makeTree() {
  m_tree = new TTree("SMOG","SMOG");
  // m_tree = new TTree("Smog", ""
  //   "SCT: Self-describing Compact Tree\n"
  //   "\n"
  //   "SCT is a custom event format used by MPIK Heidelberg for LHCb data analysis.\n"
  //   "It was designed towards fast read access, small size, and simple usage.\n"
  //   "Stored is event data, trigger data, vertex and track data, and optionally a MC record.\n"
  //   "\n"
  //   "Physical measurements for tracks and vertices are stored in LHCb internal units.\n"
  //   "position (x,y,z): millimeter\n"
  //   "momentum (px, py, pz): MeV/c\n"
  //   "mass (m): MeV/c^2\n"
  //   "charge (q): multiples of elementary charge\n"
  //   "\n"
  //   "Covariance matrices use the same units.\n"
  //   "\n"
  //   "The MC record is constructed by followig the particle history from the primary vertex\n"
  //   "until the lifetime of a particle is larger 1 ns or if it does not have any daughters\n"
  //   "(not counting secondary interactions). In addition, all particles associated to long\n"
  //   "tracks are stored together with their mothers.\n"
  //   "\n"
  //   "Branches:\n"
  //   "evt_run        : run number\n"
  //   "evt_evnum      : event number\n"
  //   "evt_bxtype     : bunch crossing type (see enum LHCb::ODIN::BXTypes)\n"
  //   "evt_trtype     : trigger type (see enum LHCb::ODIN::TriggerType)\n"
  //   "evt_bunchid    : bunch ID\n"
  //   "evt_mag_pol    : magnetic field is -1: pointing down, 1: pointing up, 0: unknown\n"
  //   "\n"
  //   "trig_tck       : trigger configuration key\n"
  //   "trig_bits      : trigger bits for this event, see HLT map below\n"
  //   "\n"
  //   "tracks_*       : track counters, see https://twiki.cern.ch/twiki/bin/view/LHCb/LHCbTrackingStrategies#TrackTypes\n"
  //   "\n"
  //   "hits_*         : hit counters for sub-detectors\n"
  //   "\n"
  //   "vtx_len        : number of primary vertices in this event\n"
  //   "vtx_[x,y,z]    : position\n"
  //   "vtx_[chi2,ndf] : fit quality\n"
  //   "vtx_cov        : covariance matrix for (x, y, z)\n"
  //   "\n"
  //   "trk_len        : number of long tracks\n"
  //   "trk_[x,y,z]    : position\n"
  //   "trk_p[x,y,z]   : momentum\n"
  //   "trk_q          : charge\n"
  //   "trk_type       : track type, see enum LHCb::Track::Types\n"
  //   "trk_[chi2,ndf] : fit quality\n"
  //   "trk_dpovp      : relative momentum resolution\n"
  //   "trk_pghost     : ghost probability\n"
  //   "trk_dllk       : delta log-likelihood to be kaon\n"
  //   "trk_dllp       : delta log-likelihood to be proton\n"
  //   "trk_dllmu      : delta log-likelihood to be muon\n"
  //   "trk_cov        : covariance matrix for (x, y, z, px, py, pz)\n"
  //   "trk_imc        : index of associated MC particle\n"
  //   "\n"
  //   "vtrk_len       : number of velo-like tracks (no momentum data)\n"
  //   "vtrk_[x,y,z]   : position\n"
  //   "vtrk_p[x,y,z]  : momentum (only direction)\n"
  //   "vtrk_[chi2,ndf]: fit quality\n"
  //   "vtrk_type      : track type, see enum LHCb::Track::Types\n"
  //   "vtrk_cov       : covariance matrix for (x, y, z, px, py, pz)\n"
  //   "\n"
  //   "mc_vtx_len     : number of MC primary vertices\n"
  //   "mc_vtx_[x,y,z] : position\n"
  //   "mc_vtx_type    : vertex type, see enum LHCb::MCVertex::MCVertexType\n"
  //   "\n"
  //   "mc_trk_len     : number of MC particles\n"
  //   "mc_trk_[x,y,z] : position\n"
  //   "mc_trk_p[x,y,z]: momentum\n"
  //   "mc_trk_q       : charge\n"
  //   "mc_trk_m       : virtual mass = (energy^2-p^2)^0.5\n"
  //   "mc_trk_flag    : bit field [0: final state, 1: part of association chain, 2: track-associated]\n"
  //   "mc_trk_pid     : PDG ID\n"
  //   "mc_trk_ldecay  : travel distance in lab frame until decay\n"
  //   "mc_trk_tsum    : sum of life times of mothers\n"
  //   "mc_trk_tmax    : maximum of life times of mothers\n"
  //   "mc_trk_ivtx    : index of associated primary vertex\n"
  // );
  m_vtx_mgr.set(m_tree);
  m_trk_mgr.set(m_tree);
  m_vtrk_mgr.set(m_tree);
  m_mc_vtx_mgr.set(m_tree);
  m_mc_trk_mgr.set(m_tree);

  branch(m_tree, "evt_run", &evt_run);
  branch(m_tree, "evt_evnum", &evt_evnum);
  branch(m_tree, "evt_bxtype", &evt_bxtype);
  branch(m_tree, "evt_trgtype", &evt_trgtype);
  branch(m_tree, "evt_bunchid", &evt_bunchid);
  branch(m_tree, "evt_mag_pol", &evt_mag_pol);

  branch(m_tree, "trig_tck", &trig_tck);
  m_tree->Branch("trig_bits", &trig_bits, "trig_bits[192]/O");

  branch(m_tree, "tracks_long", &tracks_long);
  branch(m_tree, "tracks_down", &tracks_down);
  branch(m_tree, "tracks_up", &tracks_up);
  branch(m_tree, "tracks_velo", &tracks_velo);
  branch(m_tree, "tracks_t", &tracks_t);
  branch(m_tree, "tracks_back", &tracks_back);
  branch(m_tree, "tracks_any", &tracks_any);
  branch(m_tree, "tracks_ghost", &tracks_ghost);
  branch(m_tree, "tracks_muon", &tracks_muon);

  if (m_lumi) {
    branch(m_tree, "lumi_lc3", &lumi_lc3);
    branch(m_tree, "lumi_lc3b", &lumi_lc3b);
    branch(m_tree, "lumi_lc14", &lumi_lc14);
  }

  branch(m_tree, "hits_velo", &hits_velo);
  branch(m_tree, "hits_tt", &hits_tt);
  branch(m_tree, "hits_it", &hits_it);
  branch(m_tree, "hits_ot", &hits_ot);
  branch(m_tree, "hits_spd", &hits_spd);
  branch(m_tree, "hits_rich1", &hits_rich1);
  branch(m_tree, "hits_rich2", &hits_rich2);
  branch(m_tree, "hits_muon", &hits_muon);

  m_vtx_mgr.size_branch("vtx_len");
  m_vtx_mgr.branch("vtx_x", &vtx_x);
  m_vtx_mgr.branch("vtx_y", &vtx_y);
  m_vtx_mgr.branch("vtx_z", &vtx_z);
  m_vtx_mgr.branch("vtx_chi2", &vtx_chi2);
  m_vtx_mgr.branch("vtx_ndf", &vtx_ndf);
  m_vtx_mgr.branch("vtx_ntrk", &vtx_ntrk);
  m_vtx_mgr.branch("vtx_sel", &vtx_sel);
  m_vtx_mgr.branch("vtx_smogtopo", &vtx_smogtopo);
  if (m_write_cov)
    m_vtx_mgr.branch("vtx_cov", &vtx_cov, "TMatrixFSym");
 if (m_MC) {
   m_vtx_mgr.branch("vtx_mcz", &vtx_mcz);
 }

  m_trk_mgr.size_branch("trk_len");
  m_trk_mgr.branch("trk_px", &trk_px);
  m_trk_mgr.branch("trk_py", &trk_py);
  m_trk_mgr.branch("trk_pz", &trk_pz);
  m_trk_mgr.branch("trk_q", &trk_q);
  m_trk_mgr.branch("trk_chi2", &trk_chi2);
  m_trk_mgr.branch("trk_ndf", &trk_ndf);
  m_trk_mgr.branch("trk_dpovp", &trk_dpovp);
  m_trk_mgr.branch("trk_pghost", &trk_pghost);
  m_trk_mgr.branch("trk_dllk", &trk_dllk);
  m_trk_mgr.branch("trk_dllp", &trk_dllp);
  m_trk_mgr.branch("trk_dllmu", &trk_dllmu);
  m_trk_mgr.branch("trk_selpid", &trk_selpid);
  m_trk_mgr.branch("trk_pvz", &trk_pvz);
  m_trk_mgr.branch("trk_ip", &trk_ip);
  m_trk_mgr.branch("trk_ipch2", &trk_ipch2);
  if (m_probnnx) {
    m_trk_mgr.branch("trk_pnnk", &trk_pnnk);
    m_trk_mgr.branch("trk_pnnmu", &trk_pnnmu);
  }
  if (m_write_cov)
    m_trk_mgr.branch("trk_cov", &trk_cov, "TMatrixFSym");

  if(m_velotrk) {
  m_vtrk_mgr.size_branch("vtrk_len");
  m_vtrk_mgr.branch("vtrk_x", &vtrk_x);
  m_vtrk_mgr.branch("vtrk_y", &vtrk_y);
  m_vtrk_mgr.branch("vtrk_z", &vtrk_z);
  m_vtrk_mgr.branch("vtrk_px", &vtrk_px);
  m_vtrk_mgr.branch("vtrk_py", &vtrk_py);
  m_vtrk_mgr.branch("vtrk_pz", &vtrk_pz);
  m_vtrk_mgr.branch("vtrk_chi2", &vtrk_chi2);
  m_vtrk_mgr.branch("vtrk_ndf", &vtrk_ndf);
  m_vtrk_mgr.branch("vtrk_type", &vtrk_type);
  if (m_write_cov)
    m_vtrk_mgr.branch("vtrk_cov", &vtrk_cov, "TMatrixFSym");
  }

  if (m_MC) {
    m_trk_mgr.branch("trk_mcindx1", &trk_mcindx1);
    m_trk_mgr.branch("trk_mcindx2", &trk_mcindx2);
     m_trk_mgr.branch("trk_mcpid", &trk_mcpid);

    m_mc_vtx_mgr.size_branch("mc_vtx_len");
    m_mc_vtx_mgr.branch("mc_vtx_x", &mc_vtx_x);
    m_mc_vtx_mgr.branch("mc_vtx_y", &mc_vtx_y);
    m_mc_vtx_mgr.branch("mc_vtx_z", &mc_vtx_z);
    m_mc_vtx_mgr.branch("mc_vtx_type", &mc_vtx_type);

    m_mc_trk_mgr.size_branch("mc_trk_len");
    m_mc_trk_mgr.branch("mc_trk_x", &mc_trk_x);
    m_mc_trk_mgr.branch("mc_trk_y", &mc_trk_y);
    m_mc_trk_mgr.branch("mc_trk_z", &mc_trk_z);
    m_mc_trk_mgr.branch("mc_trk_pvz", &mc_trk_pvz);
    m_mc_trk_mgr.branch("mc_trk_px", &mc_trk_px);
    m_mc_trk_mgr.branch("mc_trk_py", &mc_trk_py);
    m_mc_trk_mgr.branch("mc_trk_pz", &mc_trk_pz);
    m_mc_trk_mgr.branch("mc_trk_pid", &mc_trk_pid);
    m_mc_trk_mgr.branch("mc_trk_mpid", &mc_trk_mpid);
    m_mc_trk_mgr.branch("mc_trk_mmpid", &mc_trk_mmpid);
    m_mc_trk_mgr.branch("mc_trk_msel", &mc_trk_msel);
    m_mc_trk_mgr.branch("mc_trk_rindx1", &mc_trk_rindx1);
    m_mc_trk_mgr.branch("mc_trk_rindx2", &mc_trk_rindx2);
    m_mc_trk_mgr.branch("mc_trk_store", &mc_trk_store);
  }

  m_vtx_mgr.resize(16);
  m_mc_vtx_mgr.resize(16);
  m_trk_mgr.resize(32);
  m_vtrk_mgr.resize(32);
  m_mc_trk_mgr.resize(32);
}

StatusCode SmogAnalysis::initialize() {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  StatusCode sc = DaVinciTupleAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  // linker to MC particle and collision type
  if (m_MC) {
  m_coll_tool = tool<IMC2Collision>("LoKi::MC2GenCollision", this);
  m_pLinker   = new Particle2MCLinker(this, Particle2MCMethod::Links, "");
  m_recible = tool<IMCReconstructible>("MCReconstructible");
  m_rected  = tool<IMCReconstructed>("MCReconstructed"); 
  m_pLinkerProto = new ProtoParticle2MCLinker( this, Particle2MCMethod::ChargedPP, LHCb::ProtoParticleLocation::Charged);

  // reco to MC associator
 for ( std::vector<std::string>::const_iterator iMCAss = m_p2mcAssocTypes.begin();
	        iMCAss != m_p2mcAssocTypes.end(); ++iMCAss )
  {
    //cout<<"--- associate "<<*iMCAss<<endl;
	    m_p2mcAssocs.push_back( tool<IParticle2MCAssociator>(*iMCAss,this) );
	  }
 
 if(m_p2mcAssocs.empty())  error() <<"****no mcassociators "<< endmsg;
 
  }

  // tools
  m_ppSvc     = ppSvc();
  m_magnetSvc = service("MagneticFieldSvc", true).as<ILHCbMagnetSvc>();
  if (!m_magnetSvc) {
    error() << "******Could not retrieve MagneticFieldSvc" << endmsg;
  }
  // VELO descriptor
  fvelo = getDet<DeVelo>( DeVeloLocation::Default  );
//
  
  m_file = TFile::Open(m_filename.c_str(), "RECREATE");
  if (!m_file)
    throw std::runtime_error("*****file creation failed");

  makeTree();
  // m_tree->SetAutoSave(-100000000); // save tree every 100 MB

  // histos
  thehall=new MyHisto("a",m_MC);     // all charged particles(no duplicates)
 thehsel=new MyHisto("s",m_MC);     // selected tracks
 thehselpb=new MyHisto("spb",m_MC); // selected pb, cuts
 thehselp=new MyHisto("sp",m_MC);   // selected p, cuts
 thehselpi=new MyHisto("spi",m_MC); // selected pi, cuts
 thehselk=new MyHisto("sk",m_MC);   // selected pi, cuts

 if(m_MC) {
   thehallmcpb=new MyHisto("mcpb",1); // by pid
   thehallmcp=new MyHisto("mcp",1);
   thehallmcpi=new MyHisto("mcpi",1);
   thehallmck=new MyHisto("mck",1);
   
   thehaccmcpb=new MyHisto("mcaccpb",1);  // mcpb in acc
   thehricmcpb=new MyHisto("mcricpb",1);  // selected
   thehrecmcpb=new MyHisto("mcrecpb",1);  // reconstructed

   thehaccmck=new MyHisto("mcacck",1);  // mcpb in acc
   thehricmck=new MyHisto("mcrick",1);  // selected
   thehrecmck=new MyHisto("mcreck",1);  // reconstructed


 }
 // event histo
thehevall=new MyEvHisto("all",m_MC);
thehevsel=new MyEvHisto("sel",m_MC);


  return StatusCode::SUCCESS;
}

StatusCode SmogAnalysis::execute() {
  if (msgLevel(MSG::DEBUG)) debug() << "==> Execute" << endmsg;
 
  if(!m_MC) fselectsmog=selectSmog();

  fillEventHisto();
  // only for data
  if(m_smog & !m_MC & !fselectsmog) return StatusCode::SUCCESS;
  //cout<<" selectsmog "<<fselectsmog<< endl;

 if (msgLevel(MSG::DEBUG))   debug() <<"++ selectsmog "<<fselectsmog<< endmsg;
  copyEventData();

  if (m_lumi)
    copyLumiData();

  copyVertexData();

  copyTrackData();

   if (m_velotrk)  
     copyVeloTrackData();

  if (m_MC)
    copyMCData();

  matchIndx();

  m_tree->Fill();

  setFilterPassed(true); // Mandatory. Set to true if event is accepted.

  return StatusCode::SUCCESS;
}

StatusCode SmogAnalysis::finalize() {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  m_file->cd();
  m_tree->Write();
     if(thehall)    thehall->write(m_file);
     if(thehsel)  thehsel->write(m_file);
     if(thehselp)  thehselp->write(m_file);
     if(thehselpb)  thehselpb->write(m_file);
     if(thehselpi) thehselpi->write(m_file);
     if(thehselk)  thehselk->write(m_file);
     if(thehallmc) thehallmc->write(m_file);
     if(thehallmcpb) thehallmcpb->write(m_file);
     if(thehallmcp) thehallmcp->write(m_file);
     if(thehallmcpi) thehallmcpi->write(m_file);
     if(thehallmck) thehallmck->write(m_file);
 
     if(thehaccmcpb) thehaccmcpb->write(m_file);
     if(thehricmcpb) thehricmcpb->write(m_file);
     if(thehrecmcpb) thehrecmcpb->write(m_file);

    if(thehaccmck) thehaccmck->write(m_file);
     if(thehricmck) thehricmck->write(m_file);
     if(thehrecmck) thehrecmck->write(m_file);



     if(thehevall)  thehevall->write(m_file);
     if(thehevsel)  thehevsel->write(m_file);

     if(m_file) m_file->Close();
  return DaVinciTupleAlgorithm::finalize();  // must be called after all other actions
}

void SmogAnalysis::fillEventHisto() {

 LHCb::RecSummary *rs = getIfExists<LHCb::RecSummary>(evtSvc(), LHCb::RecSummaryLocation::Default);
   if(!rs)  return;

int  npv = rs->info(LHCb::RecSummary::nPVs,-1);
int  nlongtracks = rs->info(LHCb::RecSummary::nLongTracks,-1);
int  nbacktracks = rs->info(LHCb::RecSummary::nBackTracks,-1);
int  nupstreamtracks =  rs->info(LHCb::RecSummary::nUpstreamTracks,-1);
int  nmuons=rs->info(LHCb::RecSummary::nMuonTracks,-1);
thehevall->fillev(nlongtracks,nbacktracks,nupstreamtracks,npv,nmuons);
if(fselectsmog) thehevsel->fillev(nlongtracks,nbacktracks,nupstreamtracks,npv,nmuons);


const LHCb::RecVertex::Range vprims = this->primaryVertices() ;
for ( LHCb::RecVertex::Range::const_iterator ipv = vprims.begin() ; 
        ipv !=vprims.end() ; ++ipv ){
  //const SmartRefVector< LHCb::Track > & PVtracks = (*ipv)->tracks();
  int npvtracks=(*ipv)->tracks().size();
 double smogtopo=smogTopo((*ipv));
 thehevall->fillvtx((*ipv)->position().z(),smogtopo,npvtracks);
if(fselectsmog) thehevsel->fillvtx((*ipv)->position().z(),smogtopo,npvtracks);
  }
// all long tracks
 for(LHCb::Tracks::const_iterator
        it = get<LHCb::Tracks>(LHCb::TrackLocation::Default)->begin(),
        end = get<LHCb::Tracks>(LHCb::TrackLocation::Default)->end();
      it != end; ++it) {
    const LHCb::Track *track = *it;
    const int type  = track->type();
    if(type!=LHCb::Track::Long) continue;
    thehevall->filltrk(track->p(),track->pt(),track->pseudoRapidity(),track->ghostProbability());
    if(fselectsmog) thehevsel->filltrk(track->p(),track->pt(),track->pseudoRapidity(),track->ghostProbability());
 }//it


 if(m_MC) {
LHCb::MCParticle::Container* part = get<LHCb::MCParticle::Container>(LHCb::MCParticleLocation::Default);
 for( LHCb::MCParticle::Container::const_iterator imc = part->begin(); imc != part->end(); ++imc){
	const LHCb::MCParticle* mcp = (*imc);
	bool bselect=true;
if(!isStableCharged(mcp)) bselect=false; 
if(!isInFiducial(mcp)) bselect=false; 
if(!m_rected->reconstructed(mcp) ) bselect=false; 

	int mpid=mcp->particleID().pid();
	if(mpid==-2212 ) {
    thehevall->fillmcpb(mcp->p(),mcp->pt(),mcp->pseudoRapidity());
     if(bselect) thehevsel->fillmcpb(mcp->p(),mcp->pt(),mcp->pseudoRapidity());

 const LHCb::MCParticle*  mmcp= mcp-> mother(); 
    if(mmcp) {
      if(abs(mmcp->particleID().pid())>2200  &&  mcp->primaryVertex()->position().z()!=mcp->originVertex()->position().z() ) { 
   thehevall->fillmcpbnp(mcp->p(),mcp->pt(),mcp->pseudoRapidity());
    if(bselect) thehevsel->fillmcpbnp(mcp->p(),mcp->pt(),mcp->pseudoRapidity());
      }
    }

    }//pb
	if(mpid==2212 ) {
    thehevall->fillmcp(mcp->p(),mcp->pt(),mcp->pseudoRapidity());
    if(bselect) thehevsel->fillmcp(mcp->p(),mcp->pt(),mcp->pseudoRapidity());
			}
      if(abs(mpid)==211 ) {
    thehevall->fillmcpi(mcp->p(),mcp->pt(),mcp->pseudoRapidity());
    if(bselect) thehevsel->fillmcpi(mcp->p(),mcp->pt(),mcp->pseudoRapidity());
			}
  if(abs(mpid)==321 ) {
    thehevall->fillmck(mcp->p(),mcp->pt(),mcp->pseudoRapidity());
    if(bselect) thehevsel->fillmck(mcp->p(),mcp->pt(),mcp->pseudoRapidity());
			}
 
   }//imc
 }// m_mc

 return;
}


void SmogAnalysis::copyEventData() {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Copying event data" << endmsg;

  // access online information from ODIN

  const LHCb::ODIN * odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);

  if (odin) {
    evt_run     = odin->runNumber();
    evt_evnum   = odin->eventNumber();
    evt_bunchid = odin->bunchId();
    evt_bxtype  = static_cast<int8_t>(odin->bunchCrossingType());
    evt_trgtype  = static_cast<int8_t>(odin->triggerType());
  } else {
    evt_run     = -1;
    evt_evnum   = -1;
    evt_bunchid = -1;
    evt_bxtype  = -1;
    evt_trgtype  = -1;
  }

  evt_mag_pol = m_magnetSvc->isDown() ? -1 : 1;

  // decode trigger information
  const LHCb::HltDecReports* dec1 = get<LHCb::HltDecReports>(LHCb::HltDecReportsLocation::Hlt1Default);
  const LHCb::HltDecReports* dec2 = get<LHCb::HltDecReports>(LHCb::HltDecReportsLocation::Hlt2Default);

  if(dec1 && dec2) {
    trig_tck = dec1->configuredTCK();

    // dump the list of triggers and check that trig_bits-buffer is large enough
    if (m_first_event) {
      m_first_event = false;
      unsigned ntrig = 0;
      std::ostringstream os;
      os << m_tree->GetTitle() << "\n";
      os << "HLT map of position in trig_bits to trigger type:";
      for (const auto& report : *dec1)
        os << "\n" << ntrig++ << ": " << report.first;
      for (const auto& report : *dec2)
        os << "\n" << ntrig++ << ": " << report.first;

      if (ntrig > (sizeof(trig_bits)/sizeof(bool))) {
        std::ostringstream msg;
        msg << "more trigger decisions (" << ntrig
            << ") than reserved trigger bits";
        throw std::runtime_error(msg.str());
      }
      m_tree->SetTitle(os.str().c_str());
    }

    unsigned ibit = 0;
    for(const auto& report : *dec1)
      trig_bits[ibit++] = report.second.decision();
    for(const auto& report : *dec2)
      trig_bits[ibit++] = report.second.decision();
  } else {
    trig_tck = -1;
    for (unsigned i = 0; i < sizeof(trig_bits)/sizeof(bool); ++i)
      trig_bits[i] = false;
  }

  // get the reconstruction summary

  LHCb::RecSummary *rs = getIfExists<LHCb::RecSummary>(evtSvc(), LHCb::RecSummaryLocation::Default);
  if(rs) {
    tracks_long = rs->info(LHCb::RecSummary::nLongTracks,-1);
    tracks_down = rs->info(LHCb::RecSummary::nDownstreamTracks,-1);
    tracks_up = rs->info(LHCb::RecSummary::nUpstreamTracks,-1);
    tracks_velo = rs->info(LHCb::RecSummary::nVeloTracks,-1);
    tracks_t = rs->info(LHCb::RecSummary::nTTracks,-1);
    tracks_back = rs->info(LHCb::RecSummary::nBackTracks,-1);
    tracks_any = rs->info(LHCb::RecSummary::nTracks,-1);
    tracks_ghost = rs->info(LHCb::RecSummary::nGhosts,-1);
    tracks_muon = rs->info(LHCb::RecSummary::nMuonTracks,-1);

    hits_velo  = rs->info(LHCb::RecSummary::nVeloClusters,-1);
    hits_tt    = rs->info(LHCb::RecSummary::nTTClusters,-1);
    hits_it    = rs->info(LHCb::RecSummary::nITClusters,-1);
    hits_ot    = rs->info(LHCb::RecSummary::nOTClusters,-1);
    hits_spd   = rs->info(LHCb::RecSummary::nSPDhits,-1);
    hits_rich1 = rs->info(LHCb::RecSummary::nRich1Hits,-1);
    hits_rich2 = rs->info(LHCb::RecSummary::nRich2Hits,-1);
    hits_muon  = rs->info(LHCb::RecSummary::nMuonCoordsS0,-1)
      + rs->info(LHCb::RecSummary::nMuonCoordsS1,-1);
      + rs->info(LHCb::RecSummary::nMuonCoordsS2,-1);
      + rs->info(LHCb::RecSummary::nMuonCoordsS3,-1);
      + rs->info(LHCb::RecSummary::nMuonCoordsS4,-1);
  } else {
    tracks_long  = -20;
    tracks_down  = -20;
    tracks_up    = -20;
    tracks_velo  = -20;
    tracks_t     = -20;
    tracks_back  = -20;
    tracks_any   = -20;
    tracks_ghost = -20;
    tracks_muon  = -20;

    hits_velo  = -20;
    hits_tt    = -20;
    hits_it    = -20;
    hits_ot    = -20;
    hits_spd   = -20;
    hits_rich1 = -20;
    hits_rich2 = -20;
    hits_muon  = -20;
  }
}

void SmogAnalysis::copyLumiData() {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Copying lumi data" << endmsg;

  // determine the number of velo tracks for the lumi

  lumi_lc3 = 0;
  SmartDataPtr<LHCb::Tracks> velotracks( evtSvc(), "Rec/Track/FittedHLT1VeloTracks" );
  if(velotracks) {
    for (LHCb::Tracks::const_iterator iTr = velotracks->begin(); iTr != velotracks->end(); iTr++ ) {
      const LHCb::Track *t = (*iTr);
      const double x = t->firstState().position().x();
      const double y = t->firstState().position().y();
      const double z = t->firstState().position().z();
      if(std::fabs(z)<250. && (x*x+y*y)<16.)
        ++lumi_lc3;
    }
  } else if (msgLevel(MSG::DEBUG)) {
    std::printf("*** Rec/Track/FittedHLT1VeloTracks  not found ***\n");
  }

  // determine the number of velo tracks for the best container

  lumi_lc3b = 0;
  for(LHCb::Tracks::const_iterator
        it = get<LHCb::Tracks>(LHCb::TrackLocation::Default)->begin(),
        end = get<LHCb::Tracks>(LHCb::TrackLocation::Default)->end();
      it != end; ++it) {
    const LHCb::Track *track = *it;

    const int type = track->type();
    if(!(type==LHCb::Track::Long ||
         type==LHCb::Track::Velo ||
         type==LHCb::Track::Upstream)) continue;  // those tracks also have a velo segment

    const double x = track->firstState().position().x();
    const double y = track->firstState().position().y();
    const double z = track->firstState().position().z();

    if(std::fabs(z)<250. && (x*x+y*y)<16) ++lumi_lc3b;
  }

  // determine the number of PVs for the lumi

  lumi_lc14 = 0;
  for(LHCb::RecVertex::Range::const_iterator
        ipv = primaryVertices().begin(), end = primaryVertices().end();
      ipv != end; ++ipv) {
    const double x = (*ipv)->position().x();
    const double y = (*ipv)->position().y();
    const double z = (*ipv)->position().z();
    if(std::fabs(z)<250. && (x*x+y*y)<16) ++lumi_lc14;
  }
}

void SmogAnalysis::copyVertexData() {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Copying vertex data" << endmsg;

  // reconstructed vertices

  const LHCb::RecVertex::Range prims = this->primaryVertices();

  m_vtx_mgr.reset();
  for(LHCb::RecVertex::Range::const_iterator ipv = prims.begin(); ipv!=prims.end(); ++ipv) {

// number of velo tracks in pv
   const SmartRefVector< LHCb::Track > & PVtracks = (*ipv)->tracks();
   unsigned int nVTracks(0);
    for ( SmartRefVector< LHCb::Track >::const_iterator tr = PVtracks.begin(); 
          tr != PVtracks.end() ; tr++ ){
          if( (*tr)->type()==Track::Velo || (*tr)->type()==Track::Upstream || (*tr)->type()==Track::Long )
        nVTracks += 1;
    }
    vtx_ntrk[m_vtx_mgr.pos()]    =  nVTracks;
    vtx_smogtopo[m_vtx_mgr.pos()]=smogTopo((*ipv));
    vtx_x[m_vtx_mgr.pos()]    = (*ipv)->position().x();
    vtx_y[m_vtx_mgr.pos()]    = (*ipv)->position().y();
    vtx_z[m_vtx_mgr.pos()]    = (*ipv)->position().z();
    vtx_chi2[m_vtx_mgr.pos()] = (*ipv)->chi2();
    vtx_ndf[m_vtx_mgr.pos()]  = (*ipv)->nDoF();
    if(selectPV((*ipv))) vtx_sel[m_vtx_mgr.pos()]=1; else vtx_sel[m_vtx_mgr.pos()]=0;
    if(m_MC) {
     const LHCb::MCVertex* mcpv=getMCvertex((*ipv));
     if(mcpv) {
      if(abs(mcpv->position().x()-(*ipv)->position().x()) <2.0 &&
      abs(mcpv->position().y()-(*ipv)->position().y()) <2.0 &&
      abs(mcpv->position().z()-(*ipv)->position().z())< 20.0 ) 
	vtx_mcz[m_vtx_mgr.pos()]=(*ipv)->position().z();
       else     vtx_mcz[m_vtx_mgr.pos()]=-999.;
				}
    }

    if (m_write_cov) {
      new ((*vtx_cov)[m_vtx_mgr.pos()]) TMatrixFSym(3);
      for (int i = 0; i < 3; ++i)
        for (int j = i; j < 3; ++j)
          static_cast<TMatrixFSym&>(*(*vtx_cov)[m_vtx_mgr.pos()])(i, j) = (*ipv)->covMatrix()(i, j);
    }
    m_vtx_mgr.increase();
  }

}

void SmogAnalysis::copyTrackData() {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Copying track data" << endmsg;

  //  Loop over reconstructed tracks
  //  here come the physics tracks
  hasProtoDuplicate(0);  // fill duplicates
  //
  rparts.clear();
  pparts.clear();
  mcrselparts.clear();
  therpart.clear();
  therproto.clear();
  m_trk_mgr.reset();
  Gaudi::XYZPoint pos;
  Gaudi::XYZVector mom;
  Gaudi::SymMatrix6x6 cov;
  double IP=0;
  double IPE=0;
  // select 
 for (LHCb::Particle::Range::const_iterator id=particles().begin(); id!=particles().end(); ++id) {
    const LHCb::Particle * part =(*id);
    const ProtoParticle* proto = part->proto();
    const LHCb::Track * track = proto->track();
    // cout<<" track "<<endl;
    if(!track) continue;
    //cout<<" track exist "<<track->p()<<endl;
    if (track->type() != LHCb::Track::Long) continue;// only long
    // cout<<" track is long  "<<track->p()<<endl;
    if(hasProtoDuplicate((*id) )) continue;          // no duplicate Proto
    //cout<<" track has no  duplicate  "<<track->p()<<endl;
    if(!(*id)->isBasicParticle() ) continue;         // decayed
    // cout<<" track is basic  "<<track->p()<<endl;
    const LHCb::RecVertex* recPV=getPVOfTrack(track, IP, IPE);
     if(!recPV) continue;                             // no associated PV
     //cout<<" track recPV "<<IP<<" "<<IPE<<endl;
int spid=pidSelect((*id));
double dllp=proto->info(LHCb::ProtoParticle::RichDLLp, -200.);
double dllk=proto->info(LHCb::ProtoParticle::RichDLLk, -200.);

if(thehall) thehall->fill(track->p(),track->pt(),track->pseudoRapidity(),
dllp,dllk,recPV->position().z(),tracks_long,1.0);

 if(!m_MC) {
   // only fpr data
    if(!isInFiducial((*id)) ) continue;              // selected
    if(!trackSelect((*id))) continue;                // track quality
 }

if(thehsel) thehsel->fill(track->p(),track->pt(),track->pseudoRapidity(),
dllp,dllk,recPV->position().z(),tracks_long,1.0);
if(spid==-2212&&thehselpb)  thehselpb->fill(track->p(),track->pt(),track->pseudoRapidity(),
dllp,dllk,recPV->position().z(),tracks_long,1.0);
if(spid==2212&&thehselp) thehselp->fill(track->p(),track->pt(),track->pseudoRapidity(),
dllp,dllk,recPV->position().z(),tracks_long,1.0);
 if(abs(spid)==211&&thehselpi) thehselpi->fill(track->p(),track->pt(),track->pseudoRapidity(),
dllp,dllk,recPV->position().z(),tracks_long,1.0); 
 if(abs(spid)==321&&thehselk) thehselk->fill(track->p(),track->pt(),track->pseudoRapidity(),
dllp,dllk,recPV->position().z(),tracks_long,1.0); 
   // 
   therpart.push_back(part);
   therproto.push_back(proto);
   rparts.push_back(std::make_pair(m_trk_mgr.pos(), part));
   pparts.push_back(std::make_pair(m_trk_mgr.pos(), proto));
  // link to MC particles, only for the selected
    if (m_MC) {
      trk_mcindx1[m_trk_mgr.pos()] = -1;   // index to final MC collection,  associator via MC
      trk_mcindx2[m_trk_mgr.pos()] = -1; // associator via reco linker
      trk_mcpid[m_trk_mgr.pos()] = -9999; // associator via reco linker
      if (m_pLinker->isAssociated(*id)) {
        mcrselparts.push_back(std::make_pair(m_trk_mgr.pos(), m_pLinker->firstMCP(*id)));

      }
        } //mc

    // get more information from the track
    track->positionAndMomentum(pos, mom, cov);
    trk_px[m_trk_mgr.pos()] = mom.x();
    trk_py[m_trk_mgr.pos()] = mom.y();
    trk_pz[m_trk_mgr.pos()] = mom.z();
    if(track->checkFlag(LHCb::Track::Backward)) {
      trk_px[m_trk_mgr.pos()] *= -1;
      trk_py[m_trk_mgr.pos()] *= -1;
      trk_pz[m_trk_mgr.pos()] *= -1;
    }
    trk_q[m_trk_mgr.pos()] = track->charge();
    trk_chi2[m_trk_mgr.pos()]   = track->chi2();
    trk_ndf[m_trk_mgr.pos()]    = track->nDoF();
    trk_pghost[m_trk_mgr.pos()] = track->ghostProbability();

 trk_ip[m_trk_mgr.pos()]      = IP;
 trk_ipch2[m_trk_mgr.pos()]   = IPE;
 trk_pvz[m_trk_mgr.pos()]     = recPV->position().z();

    if (m_write_cov) {
      new ((*trk_cov)[m_trk_mgr.pos()]) TMatrixFSym(6);
      for (int i = 0; i < 6; ++i)
        for (int j = i; j < 6; ++j)
          static_cast<TMatrixFSym&>(*(*trk_cov)[m_trk_mgr.pos()])(i, j) = cov(i, j);
    }

    const double p[3] = { mom.x(), mom.y(), mom.z() };
    double pp = 0.0;
    double var_p = 0;
    for(int i=0; i<3; ++i) {
      pp += p[i] * p[i];
      for(int j=0; j<3; ++j) {
        var_p += cov(3 + i, 3 + j) * p[i] * p[j] / pp;
      }
    }
    trk_dpovp[m_trk_mgr.pos()] = std::sqrt(var_p / pp);

    // get PID information from the protoparticle
    // if -200 ,no rich
    trk_dllk[m_trk_mgr.pos()]  = proto->info(LHCb::ProtoParticle::RichDLLk, -200.);
    trk_dllp[m_trk_mgr.pos()]  = proto->info(LHCb::ProtoParticle::RichDLLp, -200.);
    trk_dllmu[m_trk_mgr.pos()] = proto->info(LHCb::ProtoParticle::CombDLLmu,-200.);
    trk_selpid[m_trk_mgr.pos()] = spid;

    if (m_probnnx) {
      trk_pnnmu[m_trk_mgr.pos()] = proto->info(LHCb::ProtoParticle::ProbNNmu, -200.);
      trk_pnnk[m_trk_mgr.pos()]  = proto->info(LHCb::ProtoParticle::ProbNNk,  -200.);
    }
    m_trk_mgr.increase();
  }
 
  return;
}

void SmogAnalysis::copyVeloTrackData() {
  // keep all tracks from the best container 
  // -----------------------------------------------------------
  m_vtrk_mgr.reset();
  Gaudi::XYZPoint pos;
  Gaudi::XYZVector mom;
  Gaudi::SymMatrix6x6 cov;
  for(LHCb::Tracks::const_iterator
        it = get<LHCb::Tracks>(LHCb::TrackLocation::Default)->begin(),
        end = get<LHCb::Tracks>(LHCb::TrackLocation::Default)->end();
      it != end; ++it) {
    const LHCb::Track *track = *it;
    const int type  = track->type();
    const double KL = track->info(LHCb::Track::CloneDist , -1.);

    if(type==LHCb::Track::Long && KL<0.) continue;  // those tracks are already stored
    if(type!=LHCb::Track::Velo && type!=LHCb::Track::Upstream) continue;  // those tracks also have a velo segment

    track->positionAndMomentum(pos, mom, cov);
    vtrk_x[m_vtrk_mgr.pos()] = pos.x();
    vtrk_y[m_vtrk_mgr.pos()] = pos.y();
    vtrk_z[m_vtrk_mgr.pos()] = pos.z();
    vtrk_px[m_vtrk_mgr.pos()] = mom.x();
    vtrk_py[m_vtrk_mgr.pos()] = mom.y();
    vtrk_pz[m_vtrk_mgr.pos()] = mom.z();
    vtrk_chi2[m_vtrk_mgr.pos()] = track->chi2();
    vtrk_ndf[m_vtrk_mgr.pos()] = track->nDoF();
    if(track->checkFlag(LHCb::Track::Backward)) {
      vtrk_px[m_vtrk_mgr.pos()] *= -1;
      vtrk_py[m_vtrk_mgr.pos()] *= -1;
      vtrk_pz[m_vtrk_mgr.pos()] *= -1;
    }
    vtrk_type[m_vtrk_mgr.pos()] = type;

    if (m_write_cov) {
      new ((*vtrk_cov)[m_vtrk_mgr.pos()]) TMatrixFSym(6);
      for (int i = 0; i < 6; ++i)
        for (int j = i; j < 6; ++j)
          static_cast<TMatrixFSym&>(*(*vtrk_cov)[m_vtrk_mgr.pos()])(i, j) = cov(i, j);
    }

    m_vtrk_mgr.increase();
  }
  return;
}
// fill MC 
void SmogAnalysis::copyMCData() {
  //==============================================================================
  // copy all mcparticles associated with reco, and the rest in the MC acceptance
  // no tracing is needed
  //==============================================================================
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Copying MC data" << endmsg;
LHCb::MCParticles* MCparts = get<LHCb::MCParticles>(LHCb::MCParticleLocation::Default);
   LHCb::MCVertices* MCvtxs = get<LHCb::MCVertices>(LHCb::MCVertexLocation::Default);
  if (!MCparts || !MCvtxs) {
     warning() << "No MC particles or no MC vertices found" << endmsg;
     return;
   }
  //
  m_mc_trk_mgr.reset();
  m_mc_vtx_mgr.reset();
  themcpart.clear();
  mcparts.clear();
  pmcselparts.clear();
 double dummy=-999;
const LHCb::MCHeader* mch = get<LHCb::MCHeader>(LHCb::MCHeaderLocation::Default);
for ( SmartRefVector< LHCb::MCVertex >::const_iterator mipv = mch->primaryVertices().begin() ;
        mipv!=mch->primaryVertices().end() ; ++mipv)
  {
      if (!(*mipv)->isPrimary()) continue;

 auto vertex_table = m_coll_tool->vertex2collision();
 const LHCb::GenCollision * collision = MC2Collision::collision((*mipv), vertex_table);
 mc_vtx_type[m_mc_vtx_mgr.pos()] = collision ? collision->processType() : -1;
 mc_vtx_x[m_mc_vtx_mgr.pos()]=(*mipv)->position().x();
 mc_vtx_y[m_mc_vtx_mgr.pos()]=(*mipv)->position().y();
 mc_vtx_z[m_mc_vtx_mgr.pos()]=(*mipv)->position().z();
 m_mc_vtx_mgr.increase();
  } // mipv
//// generic mc particles
 int bstore=0;
 LHCb::MCParticle::Container* part = get<LHCb::MCParticle::Container>(LHCb::MCParticleLocation::Default);
 for( LHCb::MCParticle::Container::const_iterator imc = part->begin(); imc != part->end(); ++imc){
	const LHCb::MCParticle* mcp = (*imc);
      if(!mcp) continue;
       bstore=0;

 mc_trk_pid[m_mc_trk_mgr.pos()] =-9999;
if(!isStableCharged(mcp)) continue; // store only p,pi K, etc
// cout<<" mcpid "<< mc_trk_pid[m_mc_trk_mgr.pos()] <<" "<<mcp->particleID().pid()<<endl;
 int mpid=mcp->particleID().pid();
     
if(mpid==-2212 &&  thehallmcpb ) thehallmcpb->fill(mcp->p(),mcp->pt(), 
                          mcp->pseudoRapidity(), dummy,dummy,
                          mcp->originVertex()->position().z(),
                          tracks_long,1.0);
if(mpid==2212 &&  thehallmcp ) thehallmcp->fill(mcp->p(),mcp->pt(), 
                          mcp->pseudoRapidity(), dummy,dummy,
                          mcp->originVertex()->position().z(),
                          tracks_long,1.0);
 if(abs(mpid)== 211 &&  thehallmcpi ) thehallmcpi->fill(mcp->p(),mcp->pt(), 
                          mcp->pseudoRapidity(), dummy,dummy,
                          mcp->originVertex()->position().z(),
                          tracks_long,1.0);
 if(abs(mpid)== 321 &&  thehallmck ) thehallmck->fill(mcp->p(),mcp->pt(), 
                          mcp->pseudoRapidity(), dummy,dummy,
                          mcp->originVertex()->position().z(),
                          tracks_long,1.0);
      // behind selection, if found association

 for(const auto& kv : mcrselparts) {
    const LHCb::MCParticle* pmco = kv.second;
    if(!pmco) continue;
    if(pmco == mcp) {bstore++;}
    }
 const ProtoParticle* mproto =  getBestProto(mcp);

 if(mproto) 
{ if (std::find(therproto.begin(), therproto.end(),mproto ) != therproto.end() ) bstore++; }
 // selection
 if(isInFiducial(mcp)) bstore++;  // get  p,pi,kaons, ions   
 


 // if(!bstore) continue;
 mc_trk_store[m_mc_trk_mgr.pos()]=bstore; 

mcparts.push_back(std::make_pair(m_mc_trk_mgr.pos(), mcp));

   int mcsel=-2;
   if(m_recible->reconstructible(mcp) ) mcsel=0; 
   if(m_rected->reconstructed(mcp) )  mcsel++;
  

   if(mpid==-2212) {
if(thehaccmcpb ) thehaccmcpb->fill(mcp->p(),mcp->pt(), 
                          mcp->pseudoRapidity(), dummy,dummy,
                          mcp->originVertex()->position().z(),
                          tracks_long,1.0);


if(m_recible->reconstructible(mcp) &&  thehricmcpb ) thehricmcpb->fill(mcp->p(),mcp->pt(), 
                          mcp->pseudoRapidity(), dummy,dummy,
                          mcp->originVertex()->position().z(),
                          tracks_long,1.0);

if(m_rected->reconstructed(mcp)  &&  thehrecmcpb ) thehrecmcpb->fill(mcp->p(),mcp->pt(), 
                          mcp->pseudoRapidity(), dummy,dummy,
                          mcp->originVertex()->position().z(),
                          tracks_long,1.0);
   }

     if(abs(mpid)==321) {
if(thehaccmck ) thehaccmck->fill(mcp->p(),mcp->pt(), 
                          mcp->pseudoRapidity(), dummy,dummy,
                          mcp->originVertex()->position().z(),
                          tracks_long,1.0);


if(m_recible->reconstructible(mcp) &&  thehricmck ) thehricmck->fill(mcp->p(),mcp->pt(), 
                          mcp->pseudoRapidity(), dummy,dummy,
                          mcp->originVertex()->position().z(),
                          tracks_long,1.0);

if(m_rected->reconstructed(mcp)  &&  thehrecmck ) thehrecmck->fill(mcp->p(),mcp->pt(), 
                          mcp->pseudoRapidity(), dummy,dummy,
                          mcp->originVertex()->position().z(),
                          tracks_long,1.0);

     }



  if(mproto) {
pmcselparts.push_back(std::make_pair(m_mc_trk_mgr.pos(), mproto));
  }
 
    mc_trk_px[m_mc_trk_mgr.pos()] = mcp->momentum().px();
    mc_trk_py[m_mc_trk_mgr.pos()] = mcp->momentum().py();
    mc_trk_pz[m_mc_trk_mgr.pos()] = mcp->momentum().pz();
    mc_trk_x[m_mc_trk_mgr.pos()] =  mcp->originVertex()->position().x();
    mc_trk_y[m_mc_trk_mgr.pos()] =  mcp->originVertex()->position().y();
    mc_trk_z[m_mc_trk_mgr.pos()] =  mcp->originVertex()->position().z();
    mc_trk_pvz[m_mc_trk_mgr.pos()] = mcp->primaryVertex()->position().z();
    mc_trk_pid[m_mc_trk_mgr.pos()] = mcp->particleID().pid();
    mc_trk_msel[m_mc_trk_mgr.pos()] = mcsel;
    const LHCb::MCParticle*  mmcp= mcp-> mother(); 
    if(mmcp) {
   mc_trk_mpid[m_mc_trk_mgr.pos()] = mmcp->particleID().pid();
       const LHCb::MCParticle*  mmmcp= mmcp-> mother(); 
           if(mmmcp) 
   mc_trk_mmpid[m_mc_trk_mgr.pos()] = mmmcp->particleID().pid();
           else  mc_trk_mmpid[m_mc_trk_mgr.pos()] =-1; 
    }else { 
mc_trk_mpid[m_mc_trk_mgr.pos()]=-1;
mc_trk_mmpid[m_mc_trk_mgr.pos()]=-1;
   }  
mc_trk_rindx1[m_mc_trk_mgr.pos()]=-1;
mc_trk_rindx2[m_mc_trk_mgr.pos()]=-1;

//cout<<"       mcpid "<< mc_trk_pid[m_mc_trk_mgr.pos()] <<" "<<mcp->particleID().pid()<<endl;

 m_mc_trk_mgr.increase();
    }// imcp

    debug() << "==> particle tree <==\n" << endmsg;
    return;
}
// match reco and mc trk index
void  SmogAnalysis::matchIndx() {
 

  //two methods
  //mc-reco  match via particle  association recopart-mcpart
  for(const auto& imcsv : mcrselparts) {
 const LHCb::MCParticle* mcsp = imcsv.second;
 if(!mcsp) continue;
       for(const auto& imcv : mcparts) {
	 const LHCb::MCParticle* mcp = imcv.second;
	 if( mcp == mcsp ) {
         mc_trk_rindx1[imcv.first] = imcsv.first; // reco index in mcparticle
         trk_mcindx1[imcsv.first] = imcv.first;     // mc indx in recopart
         trk_mcpid[imcsv.first] = mcp->particleID().pid(); 
	 } 
       }
  }
 // mc-reco match via proto association mcpart-proto
 for(const auto& ipsv : pmcselparts) {
 const LHCb::ProtoParticle* rps = ipsv.second;
 if(!rps) continue;
      for(const auto& ipv : pparts) {
      const LHCb::ProtoParticle* rp = ipv.second;
      if(rps == rp ) {
	mc_trk_rindx2[ipsv.first] =ipv.first;
        trk_mcindx2[ipv.first] = ipsv.first;
        }
      }
 }
 return;
}



// select smog
int  SmogAnalysis::selectSmog() {
  fselectsmog=0;
  
LHCb::RecSummary *rs = getIfExists<LHCb::RecSummary>(evtSvc(), LHCb::RecSummaryLocation::Default);
int  nBackTracks=rs->info(LHCb::RecSummary::nBackTracks,-1);
int  nPVs=rs->info(LHCb::RecSummary::nPVs,-1);
int  nSPDhits= rs->info(LHCb::RecSummary::nSPDhits,-1);

//// beamtype, 1 for smog beam
 int bcid=-1;
 int bctype=-1; //0 nobeam, 1 beam1, 2 beam2  3 coll
 int run=-1;
 int ev=0;
 int trg=-1;
 
const LHCb::ODIN * odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
  if( odin ){
    bcid=odin->bunchId(); 
    bctype = odin->bunchCrossingType() ;
    run = odin->runNumber();
    ev = odin->eventNumber();
    trg = odin->triggerType() ;
  }
  ////general
 if(!m_coll & !m_MC )
   if(nPVs>=m_minNpv && nPVs<m_maxNpv && nBackTracks < m_maxNbktrk && bctype==1 && nSPDhits>0 )
      fselectsmog=1;
   else return  0;

 if(m_coll & !m_MC ) {
  if(nPVs>=m_minNpv && nPVs<m_maxNpv &&  bctype==3) 
      fselectsmog=1;    else return 0;
 }
 //3cout<<" event "<<m_coll<<" "<<nPVs<<" "<<bctype<<" "<<fselectsmog<<endl;

 // for mc , obsolote from 2305
 if(m_MC) {
  if(nPVs>=m_minNpv && nPVs<m_maxNpv) 
     fselectsmog=1; 
  else return 0;
 }

//// can mask some bcid
//// for fill 4937 runN=174625-174638
 bool bfill4937= (run>=174625 && run<=174638);
 bool bbcid1= (bcid==1112) || (bcid==1312) || (bcid==1610) || (bcid==1920) || (bcid==3034);
 //for fill 4945,  runn= 174699-174751, fill 4954 runN=175290-175302
 bool bfill4945=(run>=174699 && run<=174751);
 bool bfill4954= (run>=175290 && run<=175302);
 bool bbcid2= (bcid==1112) || (bcid==1938) || (bcid==2253) || (bcid==2814) || (bcid==3034);
 
 // bid
 if(!m_MC & !m_coll) {
  if(bfill4937 & !bbcid1)   fselectsmog=0;
  if(bfill4945 & !bbcid2)   fselectsmog=0;
  if(bfill4954 & !bbcid2)   fselectsmog=0;
}

 if(!fselectsmog)  return  0;
//// trigger
 bool btrgl1=false;
 bool btrghlt=false;
 if(!m_MC & !m_coll) 
 {  
   // hlt
  if( exist<LHCb::HltDecReports>( LHCb::HltDecReportsLocation::Hlt1Default ) ){     
    const LHCb::HltDecReports* decReports = get<LHCb::HltDecReports>( LHCb::HltDecReportsLocation::Hlt1Default );
    for(LHCb::HltDecReports::Container::const_iterator it=decReports->begin(); it!=decReports->end();++it){
      debug() << it->first << " HLT1 trigger "<<it->second.decision() <<endmsg;
      // for 2015  
      // if(it->first=="Hlt1MBMicroBiasVeloDecision")   if(it->second.decision()) btrg=true;
      //  if(it->first=="Hlt1MBMicroBiasLowMultVeloDecision")  if(it->second.decision()) btrg=true;
      //  if(it->first=="Hlt1MBNoBiasDecision")  if(it->second.decision()) btrg=true;
       // from 2016
      if(it->first=="Hlt1BEMicroBiasLowMultVeloDecision")  if(it->second.decision()) btrghlt=true;
      if(it->first=="Hlt1BEMicroBiasVeloDecision")  if(it->second.decision()) btrghlt=true;  //prescaled
      if(it->first=="Hlt1SMOGSingleTrackDecision")  if(it->second.decision()) btrghlt=true;  //prescaled
    }   
  } else warning() << "****cannot find HLT1" <<endmsg;
 }
 
  if(!btrghlt && !m_MC && !m_coll) fselectsmog=0;
  if(!fselectsmog)  return 0;
// at least one smog PV;
//<16.06 wrong selection in case of two pv, with the bad second 
bool bpv=false;
const LHCb::RecVertex::Range prims = this->primaryVertices() ;
for ( LHCb::RecVertex::Range::const_iterator ipv = prims.begin() ; 
      ipv != prims.end() ; ++ipv ) {
  bpv=selectPV((*ipv));
 }//ipv
 
if(!m_coll && !m_MC && !bpv) fselectsmog=0;
  return fselectsmog;
}

//average angle between VeloRCluster and PV
double SmogAnalysis::smogTopo(const LHCb::RecVertex* rv) {
 LHCb::VeloClusters* clusters = get<LHCb::VeloClusters>( LHCb::VeloClusterLocation::Default );  
  if ( !clusters) {
     warning() << "***Velo cluster container not found"<< endmsg; return -999.;
  }
  if (fvelo==0) return 0;
    
double avTh=0.;
int nTh=0;
  for ( LHCb::VeloClusters::iterator iterV = clusters->begin(); iterV != clusters->end(); ++iterV ) {
    if (false == (*iterV)->isRType() ) continue;
    unsigned int clusize=(*iterV)->size();
    Gaudi::XYZPoint clupos(0,0,0);
    for (unsigned int i=0; i<clusize; i++) {
      const int sensorID = ((*iterV)->channels())[i].sensor();
      const DeVeloSensor* sensor=  fvelo->sensor(sensorID);
      std::pair< Gaudi::XYZPoint, Gaudi::XYZPoint > limits=sensor->globalStripLimits ((*iterV)->strip(i));
      double r=(sqrt(limits.first.x()*limits.first.x()+limits.first.y()*limits.first.y())+
  	sqrt(limits.second.x()*limits.second.x()+limits.second.y()*limits.second.y()) )/2.;
      Gaudi::XYZPoint avpos((limits.first.x()+limits.second.x())/2.,
  		    (limits.first.y()+limits.second.y())/2.,0.);
      avpos *= r/avpos.R();
      avpos.SetZ((limits.first.z()+limits.second.z())/2.);
      clupos.SetXYZ(clupos.X() + avpos.X(),clupos.Y() + avpos.Y(), clupos.Z() + avpos.Z() );
    }
    clupos /= clusize;
    clupos = clupos - rv->position();
    avTh += clupos.Theta();
    nTh++;
      }// cluster loop

 if(nTh>0) avTh/=nTh;    
 
 return avTh; 
}
// smog tracks
int  SmogAnalysis::trackSelect(const LHCb::Particle * apart) {
const LHCb::Track *trk=apart->proto()->track();
 if(!trk) return -1;
 if(!apart->proto()->richPID()) return -2;
 if(!trk->checkType(LHCb::Track::Types::Long)) return -3;
 if(trk->ghostProbability()>m_maxTrkGhostP) return -4;
 if(trk->chi2PerDoF()>m_maxTrkCh2) return -5;
 return 1;
}

//// pid sel based on generic cuts
int SmogAnalysis::pidSelect(const LHCb::Particle * apart) {
const ProtoParticle* proto = apart->proto(); 
 if(!proto) return -9990;
 const LHCb::Track* trk = proto->track();
 if(!trk) return -9998; // if no associated track 
double PIDp= proto->info(LHCb::ProtoParticle::CombDLLp,-1000);
double PIDk= proto->info(LHCb::ProtoParticle::CombDLLk,-1000);
// smog note selection 
 bool bproton= (PIDp>18.0) && (PIDp-PIDk)<10.0;
 bool bkaon=   (PIDp-PIDk)<-5.0 && (PIDk>5.0);
 bool bpion=   (PIDp< -5.0) &&  (PIDk<-5.0);
 bool bghost=(trk->ghostProbability())>0.1; 
 int pID=-997;
  if(bproton & !bghost) pID=2212;
  if(bkaon & !bghost) pID=321;
  if(bpion & !bghost) pID=211;
  if(bghost) pID=1000;
  if(trk->charge()<0)  pID *=-1.;
 return pID;
}

// remove duplicate , always second
bool SmogAnalysis::hasProtoDuplicate(const LHCb::Particle *  apart)  {
  if(apart==0) 
  {
   // fill proto duplicates 
std::vector<const LHCb::ProtoParticle*> vpt;
 vpt.clear(); vpd.clear();
const LHCb::Particle::Range part = this->particles();
 int in=0;
 for ( LHCb::Particle::Range::const_iterator im =  part.begin() ;
        im != part.end() ; ++im ) {
     in++;   
    if (std::find(vpt.begin(), vpt.end(),(*im)->proto() ) != vpt.end() )  
      vpd.push_back((*im));
    else  vpt.push_back((*im)->proto());
        }//im
  }//apart
 // find duplicate proto
 if(apart) 
    if (std::find(vpd.begin(), vpd.end(), apart ) != vpd.end() ) return true;

   debug()<<" hasProtoDuplicate no duplicate "<<endmsg; 
  return false;  
}
// fiducial selection for reco and MC
bool  SmogAnalysis::isInFiducial(const LHCb::Particle * mc)  {
  if (mc->p() < m_minP) return false;
  if (mc->pt() < m_minPt) return false;
  const LHCb::Track* trk =  mc->proto() ? (mc->proto()->track() ?
                                                    mc->proto()->track() : 0) : 0;
  if(!trk) return false;
 if (trk->pseudoRapidity() < m_minEta || trk->pseudoRapidity() > m_maxEta) return false;
 double IP, IPE;
 const LHCb::RecVertex* recPV=getPVOfTrack(mc->proto()->track(), IP, IPE);
 if(!recPV) return false;
    if (recPV->position().z()  >  m_maxZ ) return false; 
    if (recPV->position().z() <   m_minZ ) return false;   
  return true;
}

bool SmogAnalysis ::isInFiducial(const LHCb::MCParticle * mc)  {
  if (mc->pseudoRapidity() < m_minEta-0.2 || mc->pseudoRapidity() > m_maxEta+0.2) return false;
  if (mc->p() < m_minP-1000.) return false;
  if (mc->pt() < m_minPt-100.) return false;
    if (mc->originVertex()) {
    if ( mc->originVertex()->position().z() >  m_maxZ+100. ) return false; 
    if ( mc->originVertex()->position().z() < m_minZ-100. ) return false;
  }
  return true;
}
// SMOG PV
int SmogAnalysis::selectPV(const LHCb::RecVertex* rv ) {
  if(!rv) return 0;
  int bpv=1;
  double pvtopo=smogTopo(rv);
  double pvz=rv->position().z();
  if(pvz<m_minZ || pvz>m_maxZ) bpv=0; // in the z range
if( abs( rv->position().x()-(m_posBeamX-pvz*m_thetaBeamX)) >m_maxdbeam ) bpv=0;// but not in the x range
if( abs( rv->position().y()-(m_posBeamY-pvz*m_thetaBeamY)) >m_maxdbeam ) bpv=0;// but not in the y range
if(bpv) if(pvtopo>m_maxsmogtopo && pvz>-50. && pvz < 50. ) bpv=0;// smogtopo for central region 
 return bpv;
}
//closest PV in SMOG
const LHCb::RecVertex* SmogAnalysis::getPVOfTrack(const LHCb::Track * track, double& IP, double& IPE) {
  if(m_coll) return getPVOfTrackColl(track, IP, IPE);

  IP=-1;
  IPE=-1;  
  const LHCb::RecVertex::Container* PV = getIfExists<LHCb::RecVertex::Container>(LHCb::RecVertexLocation::Primary); 
  if(!PV) return NULL;
  if (!PV->size()) return NULL;
  if(!track) return NULL;
  double IPmin=1000.;
  double IPEmin=1000.;
  double IP1,IPE1;
  const LHCb::RecVertex* pvref=0;
for(LHCb::RecVertex::Container::const_iterator ipv= PV->begin() ; PV->end()!=ipv ; ++ipv ) {
  StatusCode   sc = distanceCalculator()->distance(track, (*ipv), IP1, IPE1);
  if(sc) { 
   if(IP1<IPmin&&IPE1<20.0) {IPmin=IP1;IPEmin=IPE1;pvref=(*ipv); }
  }//sc
 }//ipv
 IP=IPmin;
 IPE=IPEmin;
 if( IP==1000.) return NULL;
 else return pvref;
  return NULL;
}
// get PV for tracks in  collissions
const LHCb::RecVertex*  SmogAnalysis::getPVOfTrackColl(const LHCb::Track * track,double& IP, double& IPE)  {
  const LHCb::RecVertex::Container* PV = getIfExists<LHCb::RecVertex::Container>(LHCb::RecVertexLocation::Primary);
  // cout<<" getPV coll "<<endl;
  if (!PV->size()) return NULL;
  LHCb::RecVertex::Container::const_iterator i,ibest;
  if (PV->size()==1) ibest=PV->begin();
  else {
    double zup=9999.;
    for(LHCb::RecVertex::Container::const_iterator i = PV->begin() ; PV->end()!=i ; ++i ) {
      if ( (*i)->position().Z() < zup) {
        zup=(*i)->position().Z();
        ibest=i;
      }
    }
  }


   const LHCb::RecVertex* pvref=0;
   // const SmartRefVector< LHCb::Track >& pvtrs= (*ibest)->tracks();
 //  for ( SmartRefVector< LHCb::Track >::const_iterator it = pvtrs.begin() ; it != pvtrs.end(); it++) {
 //    const LHCb::Track* thisTr= (*it);
 //    if (thisTr == track) {
 // pvref=(*ibest);
 // cout<<" found pvtrack "<<endl;  
 //  }
 //  }
  double IP1=-1;
  double IPE1=-1;
  pvref=(*ibest);
  if(pvref) {
 StatusCode   sc = distanceCalculator()->distance(track, pvref, IP1, IPE1);
 IP=IP1;
 IPE=IPE1;
 //cout<<" getPV "<<IP<<" "<<IPE1<<endl;
 return pvref;
  }

  return NULL;
}

//get Pid of MC matched 
int  SmogAnalysis::getMCpid(const LHCb::Particle * part) {
int mcPid=0;
const MCParticle* mcp=getMCpart(part);
 if(mcp) mcPid=mcp->particleID().pid();
return mcPid;
}
// MC from p2m associator
const MCParticle* SmogAnalysis::getMCpart(const LHCb::Particle * part) {
int mcPid=0; 
 const LHCb::MCParticle* mcp(NULL);
//Assert( !m_p2mcAssocs.empty(),
//        "***The DaVinci smart associator(s) have not been initialized!");
 if(m_p2mcAssocs.empty()) return mcp;

	  if ( part) {
    if (msgLevel(MSG::VERBOSE)) verbose() << "Getting related MCP to " << part << endmsg ;	    

for ( std::vector<IParticle2MCAssociator*>::const_iterator iMCAss = m_p2mcAssocs.begin();
	          iMCAss != m_p2mcAssocs.end(); ++iMCAss ) {
       mcp = (*iMCAss)->relatedMCP(part);
      if ( mcp ) break;
	    }
if (msgLevel(MSG::VERBOSE))  verbose() << "Got mcp " << mcp << endmsg ;
	  } // part
 return mcp;
}

// get closest MC primary  vertex to the RecVertex
const MCVertex*   SmogAnalysis::getMCvertex(const LHCb::RecVertex * rv)  {
const LHCb::MCVertex* mcpv(NULL);
 double mdist=1000.;
 double dist = 1000. * Gaudi::Units::mm ;
 // const Gaudi::XYZPoint vrv=rv->position();
const LHCb::MCHeader* mch = get<LHCb::MCHeader>(LHCb::MCHeaderLocation::Default);
for ( SmartRefVector< LHCb::MCVertex >::const_iterator mipv = mch->primaryVertices().begin() ;
      mipv!=mch->primaryVertices().end() ; ++mipv) {
  const Gaudi::XYZPoint vmcv=(*mipv)->position();
  StatusCode   sc = distanceCalculator()->distance ( rv , vmcv, dist) ;
  if(dist<mdist) { mdist=dist; mcpv=(*mipv);}
}
 return mcpv;
}
// Reco Proto from ProtoLinker
const LHCb::ProtoParticle* SmogAnalysis::getBestProto(const LHCb::MCParticle* mcp) {
  LHCb::ProtoParticle* pp=NULL;
  if(!m_pLinkerProto) return pp;
  double w=0, maxW=0;
  const LHCb::ProtoParticle* cpp = m_pLinkerProto->firstP(mcp,w);
  while( cpp )
  {
    if(w>maxW) pp= const_cast<LHCb::ProtoParticle*>(cpp);
    cpp = m_pLinkerProto->nextP(w);
  }
  return pp;
}
//
bool SmogAnalysis::isStableCharged(const LHCb::MCParticle * mc) {
    if(!mc) return false;
  
    //double anID=abs(mc->particleID().pid());   
    if( abs(mc->particleID().pid()) == 13  ) return true;             // muons
    if( abs(mc->particleID().pid()) ==211  ) return true;             // pions
    if( abs(mc->particleID().pid()) ==321  ) return true;             // kaons
    if( abs(mc->particleID().pid()) ==2212  ) return true;             // protons
    if( abs(mc->particleID().pid()) == 1000010020   ) return true;             // d
    if( abs(mc->particleID().pid()) == 1000010030   ) return true;             // t
    if( abs(mc->particleID().pid()) == 1000020030   ) return true;             // he3
    if( abs(mc->particleID().pid()) == 1000020040   ) return true;             // he4

    return false;
  }
//access on demand track container?
void SmogAnalysis::selectdowntracks() {
  return;
}

 
