#ifndef SMOGHISTO_H
#define SMOGHISTO_H


#include "iostream"
#include "fstream"
#include "cstdlib"
#include "string"
#include "stdexcept"
#include "sstream"
#include "map"
#include "vector"
#include "cassert"
#include "functional"
#include "algorithm"
#include "limits"


// Root
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TClonesArray.h"
#include "TObjString.h"
#include "TMatrixT.h"
#include "TMatrixFSym.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"

using namespace std;


class BaseHisto{
public:
BaseHisto(std::string name);
~BaseHisto();
// 
int fillh1(std::string name, double x, double weight);
int fillh2(std::string name, double x, double y ,double weight);
int fillh3(std::string name, double x, double y,double z,double weight); 
virtual int book()=0;
virtual int write(std::string sname);
virtual int write(TFile* f);
void clean();
std::vector<TH1D*> thevh;
std::vector<TH2D*> thevh2;
std::vector<TH3D*> thevh3; 
std::string thename;
int Debug; 
};
////
class MyHisto: public BaseHisto {
 public:
 MyHisto(std::string name);
 MyHisto(std::string name, int amc);
~MyHisto();
int book();
int fill(double p, double pt, double eta, double pidk,double pidp, double pvz, int trk, double w); 
int bmc;
};
////
class MyEvHisto: public BaseHisto {
 public:
 MyEvHisto(std::string name);
 MyEvHisto(std::string name, int amc);
~MyEvHisto();
int book();
 int fillev(int nlongtracks,int nbacktracks,int nupstreamtracks,int npv,int nmuons); 
 int fillvtx(double vz, double smogtopo, int ntrk); 
 int filltrk(double  p,  double pt, double eta, double ghost); 
 int fillmcpb(double  p,  double pt, double eta);
 int fillmcpbnp(double  p,  double pt, double eta);
 int fillmcp(double  p,  double pt, double eta);
 int fillmcpi(double  p,  double pt, double eta);
 int fillmck(double  p,  double pt, double eta);
int bmc;
};


#endif // SMOGHISTO
